using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace HeroesOfSlimeWorld
{
	[Serializable]
	public class ActerRecording
	{
		public Acter acter;
		public List<Turn> turns = new List<Turn>();
		public int currentTurnIndex;
		public int currentTurnPositionIndex;
		public string acterName;

		public ActerRecording (Acter acter, List<Turn> turns, int currentTurnIndex = 0, int currentTurnPositionIndex = 0)
		{
			this.acter = acter;
			this.turns = turns;
			this.currentTurnIndex = currentTurnIndex;
			this.currentTurnPositionIndex = currentTurnPositionIndex;
			acterName = acter.name;
		}

		public ActerRecording (ActerRecording acterRecording) : this (acterRecording.acter, new List<Turn>(acterRecording.turns), acterRecording.currentTurnIndex, acterRecording.currentTurnPositionIndex)
		{
		}

		public void TryToDoCurrentTurn ()
		{
			Turn currentTurn = turns[currentTurnIndex];
			for (currentTurnPositionIndex = currentTurnPositionIndex; currentTurnPositionIndex < currentTurn.positions.Length; currentTurnPositionIndex ++)
			{
				Vector2Int position = currentTurn.positions[currentTurnPositionIndex];
				Collider2D hitCollider = Physics2D.OverlapPoint(position, GameManager.instance.whatIsSolid);
				if (hitCollider != null)
				{
					Acter _acter = hitCollider.GetComponent<Acter>();
					if (_acter == null || _acter.name != acterName)
						return;
				}
				acter.gameObject.SetActive(true);
				acter.trs.position = (Vector2) position;
				acter.trs.eulerAngles = Vector3.forward * currentTurn.rotations[currentTurnPositionIndex];
				Physics2D.SyncTransforms();
			}
			currentTurnIndex ++;
			currentTurnPositionIndex = 0;
			if (currentTurnIndex == turns.Count)
			{
				acter.gameObject.SetActive(false);
				GameManager.instance.playingActerRecordings.Remove(this);
			}
		}

		[Serializable]
		public class Turn
		{
			public Vector2Int[] positions = new Vector2Int[0];
			public uint[] rotations = new uint[0];

			public Turn (Turn turn)
			{
				positions = new Vector2Int[turn.positions.Length];
				turn.positions.CopyTo(positions, 0);
				rotations = new uint[turn.rotations.Length];
				turn.rotations.CopyTo(rotations, 0);
			}

			public Turn (Vector2Int[] positions, uint[] rotations)
			{
				this.positions = positions;
				this.rotations = rotations;
			}
		}
	}
}