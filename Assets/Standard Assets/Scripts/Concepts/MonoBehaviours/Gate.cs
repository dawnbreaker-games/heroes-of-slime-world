using TMPro;
using System;
using UnityEngine;

namespace HeroesOfSlimeWorld
{
	public class Gate : WallTile
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public bool isOpen;
		public SpriteRenderer spriteRenderer; 
		public Color openColor;
		public Color closedColor;
		public Collider2D collider;

#if UNITY_EDITOR
		public override void OnValidate ()
		{
			base.OnValidate ();
			if (isOpen)
				Open ();
			else
				Close ();
		}
#endif

		public virtual void HandleIsOpen ()
		{
		}

		public virtual void Open ()
		{
			isOpen = true;
			spriteRenderer.color = openColor;
			collider.enabled = false;
			Collider2D[] hitColliders = Physics2D.OverlapPointAll(trs.position + Vector3.up, GameManager.Instance.whatIsSolid);
			for (int i = 0; i < hitColliders.Length; i ++)
			{
				Collider2D hitCollider = hitColliders[i];
				Acter acter = hitCollider.GetComponent<Acter>();
				if (acter != null)
					acter.ApplyGravity ();
			}
		}

		public virtual void Close ()
		{
			isOpen = false;
			spriteRenderer.color = closedColor;
			collider.enabled = true;
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			Collider2D[] hitColliders = Physics2D.OverlapPointAll(trs.position, GameManager.Instance.whatIsSolid);
			for (int i = 0; i < hitColliders.Length; i ++)
			{
				Collider2D hitCollider = hitColliders[i];
				Acter acter = hitCollider.GetComponent<Acter>();
				if (acter != null && acter != this)
					acter.Death ();
			}
		}

		public void Toggle ()
		{
			isOpen = !isOpen;
			if (isOpen)
				Open ();
			else
				Close ();
		}

		public override void InitData ()
		{
			if (data == null)
				data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetIsOpenOfData ();
		}

		void SetIsOpenOfData ()
		{
			_Data.isOpen = isOpen;
		}

		void SetIsOpenFromData ()
		{
			isOpen = _Data.isOpen;
			if (isOpen)
				Open ();
			else
				Close ();
		}
		
		[Serializable]
		public class Data : WallTile.Data
		{
			public bool isOpen;

			public override object MakeAsset ()
			{
				throw new NotImplementedException();
			}

			public override void Apply (Asset asset)
			{
				if (asset.data == null)
					asset.data = this;
				base.Apply (asset);
				Gate gate = (Gate) asset;
				gate.SetIsOpenFromData ();
			}
		}
	}
}