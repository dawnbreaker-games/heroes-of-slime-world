using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace HeroesOfSlimeWorld
{
	public class Hazard : Asset
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public LayerMask whatIKill;
		public static SortedDictionary<int, List<Acter>> actersKilledByHazardsDict = new SortedDictionary<int, List<Acter>>();
		public static SortedDictionary<int, List<Hazard>> hazardsThatKillActersDict = new SortedDictionary<int, List<Hazard>>();

		void OnEnable ()
		{
			List<Hazard> hazardsThatKillActers;
			for (int i = 0; i < 32; i ++)
			{
				if (whatIKill.ContainsLayer(i))
				{
					if (hazardsThatKillActersDict.TryGetValue(i, out hazardsThatKillActers))
						hazardsThatKillActers.Add(this);
					else
						hazardsThatKillActers = new List<Hazard>(new Hazard[] { this });
					hazardsThatKillActersDict[i] = hazardsThatKillActers;
				}
			}
		}

		void OnDisable ()
		{
			for (int i = 0; i < 32; i ++)
			{
				List<Hazard> hazardsThatKillActers;
				if (whatIKill.ContainsLayer(i) && hazardsThatKillActersDict.TryGetValue(i, out hazardsThatKillActers))
				{
					hazardsThatKillActers.Remove(this);
					if (hazardsThatKillActers.Count == 0)
						hazardsThatKillActersDict.Remove(i);
					else
						hazardsThatKillActersDict[i] = hazardsThatKillActers;
				}
			}
		}

		public void HandleKill ()
		{
			List<Acter> actersKilledByMe;
			for (int i = 0; i < 32; i ++)
			{
				if (whatIKill.ContainsLayer(i) && actersKilledByHazardsDict.TryGetValue(i, out actersKilledByMe))
				{
					for (int i2 = 0; i2 < actersKilledByMe.Count; i2 ++)
					{
						Acter acter = actersKilledByMe[i2];
						if (!acter.isDead && acter.trs != null && acter.trs.position == trs.position)
							acter.Death ();
					}
				}
			}
		}
		
		public override void InitData ()
		{
			if (data == null)
				data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Asset.Data
		{
			public override object MakeAsset ()
			{
				Hazard hazard = Instantiate(GameManager.instance.hazardPrefab, Player.instance.trs.parent);
				Apply (hazard);
				return hazard;
			}

			public override void Apply (Asset asset)
			{
				if (asset.data == null)
					asset.data = this;
				base.Apply (asset);
			}
		}
	}
}