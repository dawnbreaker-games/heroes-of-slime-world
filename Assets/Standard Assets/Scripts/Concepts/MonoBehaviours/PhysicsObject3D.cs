using Extensions;
using UnityEngine;
using System.Collections.Generic;

public class PhysicsObject3D : MonoBehaviour
{
	// public string layerName = "";
	public Transform trs;
	public Collider collider;
	public Rigidbody rigid;
	public Renderer renderer;
	public MeshFilter meshFilter;
	// [HideInInspector]
	// public string[] collidingLayers = new string[0];
	// public static Dictionary<string, List<PhysicsObject3D>> physicsObjectsLayersDict = new Dictionary<string, List<PhysicsObject3D>>();

	// public virtual void Awake ()
	// {
	// 	if (physicsObjectsLayersDict.ContainsKey(layerName))
	// 		physicsObjectsLayersDict[layerName].Add(this);
	// 	else
	// 	{
	// 		List<PhysicsObject3D> physicsObjects = new List<PhysicsObject3D>();
	// 		physicsObjects.Add(this);
	// 		physicsObjectsLayersDict.Add(layerName, physicsObjects);
	// 	}
	// }

	// public virtual void Start ()
	// {
	// 	collidingLayers = PhysicsManager3D.layerCollisionsDict[layerName];
	// 	if (collider == null)
	// 		return;
	// 	for (int i = 0; i < physicsObjectsLayersDict.Count; i ++)
	// 	{
	// 		List<PhysicsObject3D> physicsObjects = physicsObjectsLayersDict[physicsObjectsLayersDict.Keys.Get(i)];
	// 		for (int i2 = 0; i2 < physicsObjects.Count; i2 ++)
	// 		{
	// 			PhysicsObject3D physicsObject = physicsObjects[i2];
	// 			if (physicsObject.collider != null)
	// 				Physics.IgnoreCollision(collider, physicsObject.collider, !collidingLayers.Contains(physicsObject.layerName));
	// 		}
	// 	}
	// }

	// public virtual void OnDestroy ()
	// {
	// 	physicsObjectsLayersDict[layerName].Remove(this);
	// }
}