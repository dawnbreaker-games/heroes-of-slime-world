using UnityEngine;

namespace HeroesOfSlimeWorld
{
	public class Scroll : MonoBehaviour
	{
		public GameObject contentsGo;
		public Scroll[] destroyOnShow = new Scroll[0];
		public Scroll[] destroyOnHide = new Scroll[0];
		public static Scroll[] instances = new Scroll[0];

		public void ShowOrHideContents (bool show)
		{
			contentsGo.SetActive(show);
			if (show)
			{
				for (int i = 0; i < destroyOnShow.Length; i ++)
				{
					Scroll scroll = destroyOnShow[i];
					DestroyImmediate(scroll.gameObject);
				}
				destroyOnShow = new Scroll[0];
			}
			else
			{
				for (int i = 0; i < destroyOnHide.Length; i ++)
				{
					Scroll scroll = destroyOnHide[i];
					DestroyImmediate(scroll.gameObject);
				}
				destroyOnHide = new Scroll[0];
			}
		}
	}
}