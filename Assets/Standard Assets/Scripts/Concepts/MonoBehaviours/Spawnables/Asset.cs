using System;
using UnityEngine;

namespace HeroesOfSlimeWorld
{
	public class Asset : Spawnable
	{
		public object data;
		public Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public static T Get<T> (Asset.Data acterData) where T : Asset
		{
			return Get<T>(acterData, GameManager.gameStates[GameManager.gameStates.Count - 1]);
		}

		public static T Get<T> (Asset.Data acterData, GameManager.GameState gameState) where T : Asset
		{
			for (int i2 = 0; i2 < gameState.assetsDatas.Length; i2 ++)
			{
				Asset.Data assetData = gameState.assetsDatas[i2];
				GameObject assetGo = GameObject.Find(assetData.name);
				if (assetGo != null)
				{
					Asset asset = assetGo.GetComponent<Asset>();
					assetData.Apply (asset);
					return (T) asset;
				}
				else
					return (T) assetData.MakeAsset();
			}
			return null;
		}

		public virtual void InitData ()
		{
			if (data == null)
				data = new Data();
		}

		public virtual void SetData ()
		{
			InitData ();
			SetNameOfData ();
		}

		void SetNameOfData ()
		{
			_Data.name = name;
		}

		void SetNameFromData ()
		{
			name = _Data.name;
		}

		[Serializable]
		public class Data
		{
			public string name;

			public virtual object MakeAsset ()
			{
				throw new NotImplementedException();
			}

			public virtual void Apply (Asset asset)
			{
				if (asset.data == null)
					asset.data = this;
				asset.SetNameFromData ();
			}
		}
	}
}