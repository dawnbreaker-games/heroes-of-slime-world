using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace HeroesOfSlimeWorld
{
	public class Acter : Asset
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Hazard hazard;
		public uint order;
		public LayerMask whatICrashInto;
		[HideInInspector]
		public bool isDead;
		[HideInInspector]
		[SaveAndLoadValue]
		public Portal justTeleportedFrom;
		[HideInInspector]
		public List<Vector2Int> cellsEnteredThisTurn = new List<Vector2Int>();
		[HideInInspector]
		public List<uint> rotationsThisTurn = new List<uint>();
		[HideInInspector]
		public uint fellUnitsThisTurn;
		public Acter[] connectedActers = new Acter[0];
		[HideInInspector]
		public Gel[] stuckToGels = new Gel[0];
		public bool updateConnectedActers;
		[HideInInspector]
		public bool actedThisTurn;
		public static SortedDictionary<uint, List<Acter>> instancesDict = new SortedDictionary<uint, List<Acter>>();
		public const uint LAST_ACTER_ORDER = 1;

#if UNITY_EDITOR
		public virtual void OnValidate ()
		{
			if (updateConnectedActers)
			{
				updateConnectedActers = false;
				Gel[] allGels = FindObjectsOfType<Gel>();
				for (int i = 0; i < allGels.Length; i ++)
				{
					Gel gel = allGels[i];
					for (int i2 = 0; i2 < connectedActers.Length; i2 ++)
					{
						Acter acter = connectedActers[i2];
						if (gel.stuckTo == null || gel.stuckTo == acter || gel.stuckTo2 == null || gel.stuckTo2 == acter)
						{
							GameManager.DestroyOnNextEditorUpdate (gel.gameObject);
							break;
						}
					}
				}
				for (int i = 0; i < connectedActers.Length; i ++)
				{
					Acter acter = connectedActers[i];
					acter.stuckToGels = new Gel[0];
					for (int i2 = i + 1; i2 < connectedActers.Length; i2 ++)
					{
						Acter acter2 = connectedActers[i2];
						if ((acter.trs.position - acter2.trs.position).sqrMagnitude <= 1)
						{
							Gel gel;
							gel = (Gel) PrefabUtility.InstantiatePrefab(GameManager.Instance.gelPrefab);
							gel.trs.SetParent(Player.Instance.trs.parent);
							gel.stuckTo = acter;
							gel.stuckTo2 = acter2;
							gel.OnValidate ();
						}
					}
				}
				connectedActers = connectedActers.Remove(this);
				for (int i = 0; i < connectedActers.Length; i ++)
				{
					Acter acter = connectedActers[i];
					if (acter != null)
						acter.connectedActers = connectedActers.Add(this).Remove(acter);
					else
					{
						connectedActers.RemoveAt(i);
						i --;
					}
				}
			}
		}
#endif

		public virtual void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			Register ();
			actedThisTurn = false;
		}

		public virtual void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			Deregister ();
		}

		public void Register ()
		{
			List<Acter> instances;
			if (instancesDict.TryGetValue(order, out instances))
				instances.Add(this);
			else
				instances = new List<Acter>(new Acter[] { this });
			instancesDict[order] = instances;
			List<Acter> actersKilledByHazards;
			if (Hazard.actersKilledByHazardsDict.TryGetValue(gameObject.layer, out actersKilledByHazards))
				actersKilledByHazards.Add(this);
			else
				actersKilledByHazards = new List<Acter>(new Acter[] { this });
			Hazard.actersKilledByHazardsDict[gameObject.layer] = actersKilledByHazards;
		}

		public void Deregister ()
		{
			List<Acter> instances;
			if (instancesDict.TryGetValue(order, out instances))
			{
				instances.Remove(this);
				if (instances.Count == 0)
					instancesDict.Remove(order);
				else
					instancesDict[order] = instances;
			}
			List<Acter> actersKilledByHazards;
			if (Hazard.actersKilledByHazardsDict.TryGetValue(gameObject.layer, out actersKilledByHazards))
			{
				actersKilledByHazards.Remove(this);
				if (actersKilledByHazards.Count == 0)
					Hazard.actersKilledByHazardsDict.Remove(gameObject.layer);
				else
					Hazard.actersKilledByHazardsDict[gameObject.layer] = actersKilledByHazards;
			}
		}

		public virtual void Act ()
		{
			if (!GameManager.AddStackEntry())
				return;
			if (isDead)
				return;
			HandleTeleportAt (trs.position.ToVec2Int());
			ApplyGravity ();
			if (isDead)
				return;
			if (Physics2D.OverlapPoint(trs.position, LayerMask.GetMask("Portal")) == null)
				justTeleportedFrom = null;
			actedThisTurn = true;
		}

		public void HandleMovingGround ()
		{
			if (!GameManager.AddStackEntry())
				return;
			Collider2D hitMoveTileCollider = Physics2D.OverlapPoint(trs.position + Vector3.down, LayerMask.GetMask("Move Tile"));
			if (hitMoveTileCollider != null)
			{
				MoveTile moveTile = hitMoveTileCollider.GetComponent<MoveTile>();
				if (moveTile.onMove == null)
					moveTile.onMove = () => { TryToMoveTo ((trs.position + moveTile.trs.up).ToVec2Int()); };
				else
				{
					moveTile.onMove -= () => { TryToMoveTo ((trs.position + moveTile.trs.up).ToVec2Int()); };
					moveTile.onMove += () => { TryToMoveTo ((trs.position + moveTile.trs.up).ToVec2Int()); };
				}
			}
			// TODO: Handle the moving ground of pushers
		}

		public virtual void ApplyGravity ()
		{
			if (!GameManager.AddStackEntry())
				return;
			if (isDead || trs == null)
				return;
			bool moved = true;
			for (uint i = 0; i <= GameManager.HIGHEST_FALL; i ++)
			{
				moved = TryToMoveTo(trs.position.ToVec2Int() + Vector2Int.down);
				if (isDead)
					return;
				HandleDestroyFragileWall ();
				if (isDead)
					return;
				if (hazard != null)
					hazard.HandleKill ();
				if (moved)
					fellUnitsThisTurn ++;
				else
					return;
			}
		}

		public void HandleDestroyFragileWall ()
		{
			if (!GameManager.AddStackEntry())
				return;
			if (isDead)
				return;
			HandleTeleportAt (trs.position.ToVec2Int());
			if (isDead)
				return;
			Collider2D hitFragileWallCollider = Physics2D.OverlapPoint(trs.position + Vector3.down, LayerMask.GetMask("Fragile Wall"));
			if (hitFragileWallCollider != null && fellUnitsThisTurn > 0)
			{
				Collider2D[] hitPortalColliders = Physics2D.OverlapPointAll(trs.position, LayerMask.GetMask("Portal"));
				if (hitPortalColliders.Length == 0 || hitPortalColliders.Length > 1 || hitPortalColliders[0].GetComponent<Portal>().teleportTo != justTeleportedFrom)
					hitFragileWallCollider.GetComponent<FragileWallTile>().Death ();
			}
		}

		public void HandleDeath ()
		{
			if (!GameManager.AddStackEntry())
				return;
			List<Hazard> hazardsThatKillMe;
			if (Hazard.hazardsThatKillActersDict.TryGetValue(gameObject.layer, out hazardsThatKillMe))
			{
				for (int i = 0; i < hazardsThatKillMe.Count; i ++)
				{
					Hazard hazard = hazardsThatKillMe[i];
					if (hazard.trs.position == trs.position)
					{
						Death ();
						return;
					}
				}
			}
		}

		public virtual void OnBeforeDeath ()
		{
			if (!GameManager.AddStackEntry())
				return;
			Portal[] childrenPortals = GetComponentsInChildren<Portal>();
			for (int i = 0; i < childrenPortals.Length; i ++)
			{
				Portal portal = childrenPortals[i];
				portal.trs.SetParent(trs.parent);
				portal.lineRendererToParent.enabled = false;
			}
		}

		public virtual void Death ()
		{
			if (!GameManager.AddStackEntry())
				return;
			OnBeforeDeath ();
			isDead = true;
			DestroyImmediate(gameObject);
			GameManager.diedCount ++;
			for (int i = 0; i < DeathGate.instances.Count; i ++)
			{
				DeathGate deathGate = DeathGate.instances[i];
				deathGate.HandleIsOpen ();
			}
		}

		public virtual bool CanMoveTo (Vector2Int position)
		{
			if (!GameManager.AddStackEntry())
				return false;
			bool output = false;
			Collider2D hitCollider = Physics2D.OverlapPoint(position, whatICrashInto);
			Box box = null;
			if (hitCollider == null)
				output = true;
			else
			{
				box = hitCollider.GetComponent<Box>();
				if (box != null)
				{
					if (trs.position.x > position.x)
						output = box.CanMoveTo((position + Vector2.left).ToVec2Int());
					else if (trs.position.x < position.x)
						output = box.CanMoveTo((position + Vector2.right).ToVec2Int());
				}
			}
			return output;
		}

		public virtual bool TryToMoveTo (Vector2Int position)
		{
			if (!GameManager.AddStackEntry())
				return false;
			bool output = false;
			Collider2D hitCollider = Physics2D.OverlapPoint(position, whatICrashInto);
			Box box = null;
			if (hitCollider == null)
				output = true;
			else
			{
				box = hitCollider.GetComponent<Box>();
				if (box != null)
				{
					if (trs.position.x > position.x)
						output = box.TryToMoveTo((position + Vector2.left).ToVec2Int());
					else if (trs.position.x < position.x)
						output = box.TryToMoveTo((position + Vector2.right).ToVec2Int());
				}
			}
			if (output)
			{
				SetPosition (position);
				if (box != null)
					box.Act ();
			}
			if (!isDead)
			{
				if (output)
					HandleRecordingAndPlaybackAt (position + Vector2Int.down);
				else
					HandleRecordingAndPlaybackAt (position);
				HandleMovingGround ();
				HandleDeath ();
			}
			return output;
		}

		public void SetPosition (Vector2Int position)
		{
			if (!GameManager.AddStackEntry())
				return;
			HandleChildrenPortals ();
			trs.position = position.ToVec3();
			cellsEnteredThisTurn.Add(position);
			rotationsThisTurn.Add((uint) trs.eulerAngles.z);
			Physics2D.SyncTransforms();
			HandleExplodeBomb ();
			if (isDead)
				return;
			HandleDeath ();
			if (isDead)
				return;
			if (hazard != null)
				hazard.HandleKill ();
			HandleDestroyFragileWall ();
			if (isDead)
				return;
			HandleChildrenPortals ();
			HandleTeleportAt (position);
			HandleRecordingAndPlaybackAt (position + Vector2Int.down);
		}

		public Acter[] HandleTeleportAt (Vector2Int position)
		{
			if (!GameManager.AddStackEntry())
				return new Acter[0];
			List<Acter> output = new List<Acter>();
			Collider2D[] hitPortalColliders = Physics2D.OverlapPointAll(position, LayerMask.GetMask("Portal"));
			int numberOfTeleports = 0;
			for (int i = 0; i < hitPortalColliders.Length; i ++)
			{
				Collider2D hitPortalCollider = hitPortalColliders[i];
				Portal portal = hitPortalCollider.GetComponent<Portal>();
				if ((justTeleportedFrom == null || (portal.name != justTeleportedFrom.teleportTo.name && portal.name != justTeleportedFrom.name)) && Physics2D.OverlapPoint(portal.teleportTo.trs.position, GameManager.instance.whatIsSolid) == null)
				{
					numberOfTeleports ++;
					if (numberOfTeleports > 1)
					{
						Acter acter = Instantiate(this, Player.instance.trs.parent);
						acter.name = acter.name.Replace("(Clone)", "" + numberOfTeleports);
						acter.TeleportTo (portal.teleportTo);
						output.Add(acter);
					}
					else
					{
						TeleportTo (portal.teleportTo);
						output.Add(this);
					}
				}
			}
			return output.ToArray();
		}

		void TeleportTo (Portal portal)
		{
			if (!GameManager.AddStackEntry())
				return;
			justTeleportedFrom = portal.teleportTo;
			SetPosition (portal.trs.position.ToVec2Int());
			ApplyGravity ();
			if (!isDead && Physics2D.OverlapPoint(trs.position, LayerMask.GetMask("Portal")) == null)
				justTeleportedFrom = null;
		}

		public virtual void HandleExplodeBomb ()
		{
			if (!GameManager.AddStackEntry())
				return;
			HandleTeleportAt (trs.position.ToVec2Int());
			if (isDead)
				return;
			Collider2D hitBombCollider = Physics2D.OverlapPoint(trs.position + Vector3.down, LayerMask.GetMask("Bomb"));
			if (hitBombCollider != null)
			{
				Collider2D[] hitPortalColliders = Physics2D.OverlapPointAll(trs.position, LayerMask.GetMask("Portal"));
				if (hitPortalColliders.Length == 0 || hitPortalColliders.Length > 1 || hitPortalColliders[0].GetComponent<Portal>().teleportTo != justTeleportedFrom)
					hitBombCollider.GetComponent<Bomb>().Death ();
			}
		}

		public void HandleRecordingAndPlaybackAt (Vector2Int position)
		{
			if (!GameManager.AddStackEntry())
				return;
			Collider2D hitCollider = Physics2D.OverlapPoint(position, LayerMask.GetMask("Recorder Tile"));
			if (hitCollider != null)
			{
				RecorderTile recorderTile = hitCollider.GetComponent<RecorderTile>();
				if (!recorderTile.used)
					recorderTile.StartRecording (this);
			}
			else
			{
				hitCollider = Physics2D.OverlapPoint(position, LayerMask.GetMask("Playback Tile"));
				if (hitCollider != null)
				{
					PlaybackTile playbackTile = hitCollider.GetComponent<PlaybackTile>();
					if (!playbackTile.used)
						playbackTile.StartPlaying ();
				}
			}
		}

		public void HandleChildrenPortals ()
		{
			if (!GameManager.AddStackEntry())
				return;
			Portal[] portals = GetComponentsInChildren<Portal>();
			for (int i = 0; i < portals.Length; i ++)
			{
				Portal portal = portals[i];
				portal.lineRendererToOtherPortal.SetPositions(new Vector3[] { portal.trs.position, portal.teleportTo.trs.position });
				portal.teleportTo.lineRendererToOtherPortal.SetPositions(new Vector3[] { portal.teleportTo.trs.position, portal.trs.position });
				for (uint i2 = 0; i2 <= Acter.LAST_ACTER_ORDER; i2 ++)
				{
					List<Acter> acters = new List<Acter>();
					if (Acter.instancesDict.TryGetValue(i2, out acters))
					{
						for (int i3 = 0; i3 < acters.Count; i3 ++)
						{
							Acter acter = acters[i3];
							if (!acter.isDead && acter.trs != null && acter.trs.position == portal.trs.position)
								acter.HandleTeleportAt (acter.trs.position.ToVec2Int());
						}
					}
				}
			}
		}

		public static Acter[] GetConnectedActers (Acter acter, bool includeActer = false)
		{
			List<Acter> output = new List<Acter>(new Acter[] { acter });
			List<Acter> remainingActers = new List<Acter>(output);
			while (remainingActers.Count > 0)
			{
				Acter acter2 = remainingActers[0];
				for (int i = 0; i < acter2.stuckToGels.Length; i ++)
				{
					Gel stuckToGel = acter2.stuckToGels[i];
					if (!output.Contains(stuckToGel.stuckTo))
					{
						remainingActers.Add(stuckToGel.stuckTo);
						output.Add(stuckToGel.stuckTo);
					}
					if (!output.Contains(stuckToGel.stuckTo2))
					{
						remainingActers.Add(stuckToGel.stuckTo2);
						output.Add(stuckToGel.stuckTo2);
					}
				}
				remainingActers.RemoveAt(0);
			}
			if (!includeActer)
				output.RemoveAt(0);
			return output.ToArray();
		}

		public override void InitData ()
		{
			if (data == null)
				data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetPositionOfData ();
			SetRotationOfData ();
			SetActiveOfData ();
			SetLayerOfData ();
			SetEnabledOfData ();
			SetJustTeleportedFromOfData ();
		}

		void SetPositionOfData ()
		{
			_Data.position = _Vector2Int.FromVec2Int(trs.position.ToVec2Int());
		}

		void SetPositionFromData ()
		{
			trs.position = _Data.position.ToVec2Int().ToVec3(); 
		}

		void SetRotationOfData ()
		{
			_Data.rotation = (uint) trs.eulerAngles.z;
		}

		void SetRotationFromData ()
		{
			trs.eulerAngles = Vector3.forward * _Data.rotation;
		}

		void SetActiveOfData ()
		{
			_Data.active = gameObject.activeSelf;
		}

		void SetActiveFromData ()
		{
			gameObject.SetActive(_Data.active);
		}

		void SetLayerOfData ()
		{
			_Data.layer = (uint) gameObject.layer;
		}

		void SetLayerFromData ()
		{
			gameObject.layer = (int) _Data.layer;
		}

		void SetEnabledOfData ()
		{
			_Data.enabled = enabled;
		}

		void SetEnabledFromData ()
		{
			enabled = _Data.enabled;
		}

		void SetJustTeleportedFromOfData ()
		{
			if (justTeleportedFrom != null)
				_Data.justTeleportedFromPortalName = justTeleportedFrom.name;
			else
				_Data.justTeleportedFromPortalName = null;
		}

		void SetJustTeleportedFromFromData ()
		{
			if (!string.IsNullOrEmpty(_Data.justTeleportedFromPortalName))
				justTeleportedFrom = GameObject.Find(_Data.justTeleportedFromPortalName).GetComponent<Portal>();
			else
				justTeleportedFrom = null;
		}
		
		[Serializable]
		public class Data : Asset.Data
		{
			public _Vector2Int position;
			public uint rotation;
			public bool active;
			public uint layer;
			public bool enabled;
			public string justTeleportedFromPortalName;

			public override object MakeAsset ()
			{
				throw new NotImplementedException();
			}

			public override void Apply (Asset asset)
			{
				if (asset.data == null)
					asset.data = this;
				base.Apply (asset);
				Acter acter = (Acter) asset;
				acter.SetPositionFromData ();
				acter.SetRotationFromData ();
				acter.SetActiveFromData ();
				acter.SetLayerFromData ();
				acter.SetEnabledFromData ();
				acter.SetJustTeleportedFromFromData ();
			}
		}
	}
}