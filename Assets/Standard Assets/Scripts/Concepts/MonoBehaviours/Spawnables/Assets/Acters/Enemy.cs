using System;
using Extensions;
using UnityEngine;

namespace HeroesOfSlimeWorld
{
	public class Enemy : Acter
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		protected Player targetPlayer;

		public override void Act ()
		{
			SetTargetPlayer ();
			if (Player.instances.Length == 0)
				return;
			Vector2Int move = GetDesiredMove();
			if (move != Vector2Int.zero)
			{
				TryToMoveTo (trs.position.ToVec2Int() + move);
				if (isDead)
					return;
				trs.up = (Vector2) move;
			}
			if (!isDead)
			{
				hazard.HandleKill ();
				base.Act ();
			}
		}

		public override bool TryToMoveTo (Vector2Int position)
		{
			Collider2D hitCollider = Physics2D.OverlapPoint(position, whatICrashInto);
			if (hitCollider != null)
			{
				Enemy enemy = hitCollider.GetComponent<Enemy>();
				if (enemy != null && !enemy.actedThisTurn)
				{
					enemy.SetTargetPlayer ();
					if (Player.instances.Length == 0)
						return false;
					if (enemy.GetDesiredMove() == position - trs.position.ToVec2Int())
						enemy.Act ();
				}
			}
			return base.TryToMoveTo(position);
		}

		public virtual Vector2Int GetDesiredMove ()
		{
			if (targetPlayer.trs.position.x > trs.position.x)
				return Vector2Int.right;
			else if (targetPlayer.trs.position.x < trs.position.x)
				return Vector2Int.left;
			else
				return Vector2Int.zero;
		}

		public void SetTargetPlayer ()
		{
			Player.SetInstances ();
			if (Player.instances.Length == 0)
				return;
			if (targetPlayer == null)
				targetPlayer = Player.instances[0];
			uint manhattenDistanceToTargetPlayer = VectorExtensions.GetManhattenDistance(targetPlayer.trs.position.ToVec2Int(), trs.position.ToVec2Int());
			for (int i = 0; i < Player.instances.Length; i ++)
			{
				Player player = Player.instances[i];
				uint manhattenDistanceToPlayer = VectorExtensions.GetManhattenDistance(player.trs.position.ToVec2Int(), trs.position.ToVec2Int());
				if (manhattenDistanceToPlayer < manhattenDistanceToTargetPlayer || (manhattenDistanceToPlayer == manhattenDistanceToTargetPlayer && Vector2.Angle(trs.up, player.trs.position - trs.position) < Vector2.Angle(trs.up, targetPlayer.trs.position - trs.position)))
				{
					manhattenDistanceToTargetPlayer = manhattenDistanceToPlayer;
					targetPlayer = player;
				}
			}
		}
		
		public override void InitData ()
		{
			if (data == null)
				data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Acter.Data
		{
			public override object MakeAsset ()
			{
				throw new NotImplementedException();
			}

			public override void Apply (Asset asset)
			{
				if (asset.data == null)
					asset.data = this;
				base.Apply (asset);
			}
		}
	}
}