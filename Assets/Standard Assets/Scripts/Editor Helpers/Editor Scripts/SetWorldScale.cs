#if UNITY_EDITOR
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace HeroesOfSlimeWorld
{
	[ExecuteInEditMode]
	public class SetWorldScale : EditorScript
	{
		public Transform trs;
		public Vector3 scale;
		
		public override void Do ()
		{
			trs.SetWorldScale (scale);
		}
	}
}
#else
namespace HeroesOfSlimeWorld
{
	public class SetWorldScale : EditorScript
	{
	}
}
#endif