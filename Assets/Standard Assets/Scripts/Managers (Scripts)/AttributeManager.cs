﻿using System;
using System.Reflection;
using System.Collections.Generic;
using Object = UnityEngine.Object;

namespace HeroesOfSlimeWorld
{
	public class AttributeManager : SingletonMonoBehaviour<AttributeManager>
	{
		public bool updateAttributes;
		public bool doAttributes;

		void OnValidate ()
		{
			if (updateAttributes)
			{
				updateAttributes = false;
				UpdateAttributes ();
			}
			if (doAttributes)
			{
				doAttributes = false;
				DoAttributes ();
			}
		}

		void OnEnable ()
		{
			UpdateAttributes ();
		}

		void UpdateAttributes ()
		{
			List<_Attribute> allAttributes = new List<_Attribute>();
			Object[] objects = FindObjectsOfType<Object>();
			for (int i = 0; i < objects.Length; i ++)
			{
				Object obj = objects[i];
				IAttributeHolder attributeHolder = obj as IAttributeHolder;
				if (attributeHolder != null)
				{
					Type objectType = attributeHolder.GetType();
					MemberInfo[] memberInfos = objectType.GetMembers();
					for (int i2 = 0; i2 < memberInfos.Length; i2 ++)
					{
						MemberInfo memberInfo = memberInfos[i2];
						Attribute[] attributes = Attribute.GetCustomAttributes(memberInfo, typeof(_Attribute), true);
						for (int i3 = 0; i3 < attributes.Length; i3 ++)
						{
							Attribute attribute = attributes[i3];
							_Attribute _attribute = attribute as _Attribute;
							if (_attribute != null)
							{
								allAttributes.Add(_attribute);
								Type memberType = null;
								if (memberInfo.MemberType == MemberTypes.Field)
									memberType = ((FieldInfo) memberInfo).FieldType;
								else if (memberInfo.MemberType == MemberTypes.Property)
									memberType = ((PropertyInfo) memberInfo).PropertyType;
								_attribute.Init (obj, memberType, memberInfo.Name);
							}
						}
					}
				}
			}
			_Attribute.instances = allAttributes.ToArray();
		}

		void DoAttributes ()
		{
			for (int i = 0; i < _Attribute.instances.Length; i ++)
			{
				_Attribute _attribute = _Attribute.instances[i];
				_attribute.Do ();
			}
		}
	}
}