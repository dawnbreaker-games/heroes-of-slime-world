using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Threading.Tasks;
using UnityEngine.InputSystem;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace HeroesOfSlimeWorld
{
	public class GameManager : SingletonMonoBehaviour<GameManager>, IAttributeHolder
	{
		public Player playerPrefab;
		public Scorp scorpPrefab;
		public Pusher pusherPrefab;
		public MoveTile moveTilePrefab;
		public RecorderTile recorderTilePrefab;
		public PlaybackTile playbackTilePrefab;
		public FragileWallTile fragileWallTilePrefab;
		public WallTile wallTilePrefab;
		public Box boxPrefab;
		public Bomb bombPrefab;
		public Sandworm sandwormPrefab;
		public DeathGate deathGatePrefab;
		public Gel gelPrefab;
		public Portal portalPrefab;
		public Hazard hazardPrefab;
		public Briar briarPrefab;
		public BriarSource briarSourcePrefab;
		public BriarGrowth briarGrowthPrefab;
		// public GameObject[] registeredGos = new GameObject[0];
		// [SaveAndLoadValue]
		// public GameModifier[] gameModifiers = new GameModifier[0];
		// public GameObject menuGo;
		public LayerMask whatIsSolid;
		public LayerMask whatFalls;
		public List<ActerRecording> acterRecordings = new List<ActerRecording>();
		public List<ActerRecording> recordingActerRecordings = new List<ActerRecording>();
		public List<ActerRecording> playingActerRecordings = new List<ActerRecording>();
		public List<List<ActerRecording>> acterRecordingsAtTurns = new List<List<ActerRecording>>();
		public List<List<ActerRecording>> recordingActerRecordingsAtTurns = new List<List<ActerRecording>>();		
		public List<List<ActerRecording>> playingActerRecordingsAtTurns = new List<List<ActerRecording>>();
		public static GameObject CurrentGameStateGo
		{
			get
			{
				return gameStateGos[gameStateGos.Count - 1];
			}
		}
		public static uint stackEntryCount;
		[SaveAndLoadValue]
		public static List<GameState> gameStates = new List<GameState>();
		public static List<GameObject> gameStateGos = new List<GameObject>();
		public static List<uint> diedCountAtTurns = new List<uint>();
		public static uint diedCount;
		// public static Dictionary<string, GameModifier> gameModifierDict = new Dictionary<string, GameModifier>();
		// public static bool paused;
		public static IUpdatable[] updatables = new IUpdatable[0];
		// public static int framesSinceLevelLoad;
		// public static bool isQuitting;
		// public static float pausedTime;
		// public static float TimeSinceLevelLoad
		// {
		// 	get
		// 	{
		// 		return Time.timeSinceLevelLoad - pausedTime;
		// 	}
		// }
		public const int VERSION_NUMBER = 1;
		public const uint HIGHEST_FALL = 100;
		public const uint MAX_STACK_ENTRY_COUNT = 300;
		static int currentTurnIndex = 1;
		static List<string> namesOfAlteredSpriteRenderers = new List<string>();
		static List<string> namesOfAlteredLineRenderers = new List<string>();
		static IUpdatable[] previousUpdatables = new IUpdatable[0]; 
		bool previousMenuInput;

		public override void Awake ()
		{
			if (!PlayerPrefs.HasKey("Has played"))
			{
				PlayerPrefs.DeleteAll();
				PlayerPrefsExtensions.SetBool("Has played", true);
			}
			Init ();
			base.Awake ();
			print(SaveAndLoadManager.DefaultSaveFilePath);
			currentTurnIndex = 1;
			namesOfAlteredSpriteRenderers.Clear();
			namesOfAlteredLineRenderers.Clear();
			diedCount = 0;
			diedCountAtTurns.Clear();
			gameStateGos = new List<GameObject>(new GameObject[] { Player.Instance.trs.parent.gameObject });
			diedCountAtTurns.Add(0);
			acterRecordingsAtTurns.Add(new List<ActerRecording>());
			recordingActerRecordingsAtTurns.Add(new List<ActerRecording>());
			playingActerRecordingsAtTurns.Add(new List<ActerRecording>());
			SaveAndLoadManager.Instance.Init ();
			MakeNewGameState ();
			if (instance != this)
				return;
			if (PlayerPrefs.GetInt("Version", 0) < VERSION_NUMBER)
			{
				PlayerPrefs.DeleteAll();
				PlayerPrefs.SetInt("Version", VERSION_NUMBER);
			}
			// gameModifierDict.Clear();
			// for (int i = 0; i < gameModifiers.Length; i ++)
			// {
			// 	GameModifier gameModifier = gameModifiers[i];
			// 	gameModifierDict.Add(gameModifier.name, gameModifier);
			// }
			Application.wantsToQuit += OnWantToQuit;
		}

		void Init ()
		{
			acterRecordings.Clear();
			recordingActerRecordings.Clear();
			playingActerRecordings.Clear();
			acterRecordingsAtTurns.Clear();
			recordingActerRecordingsAtTurns.Clear();
			playingActerRecordingsAtTurns.Clear();
		}

		void Update ()
		{
			stackEntryCount = 0;
			InputSystem.Update ();
			for (int i = 0; i < updatables.Length; i ++)
			{
				IUpdatable updatable = updatables[i];
				updatable.DoUpdate ();
			}
			HandleUndo ();
			HandleRestart ();
			HandleLevelSwitch ();
			// HandleMenu ();
			// framesSinceLevelLoad ++;
			// if (paused)
			// 	pausedTime += Time.unscaledDeltaTime;
			if (!enabled)
			{
				previousUpdatables.CopyTo(updatables, 0);
				enabled = true;
			}
		}

		void HandleUndo ()
		{
			if (currentTurnIndex > 2 && Keyboard.current.backspaceKey.wasPressedThisFrame)
				Undo ();
		}

		public void Undo ()
		{
			UndoToPreviousGameState ();
			UndoToPreviousGameState ();
			if (instance.acterRecordingsAtTurns.Count > currentTurnIndex)
				instance.acterRecordingsAtTurns.RemoveAt(currentTurnIndex);
			if (instance.acterRecordingsAtTurns.Count > currentTurnIndex - 1)
				instance.acterRecordings = instance.acterRecordingsAtTurns[currentTurnIndex - 1];
			if (instance.recordingActerRecordingsAtTurns.Count > currentTurnIndex)
				instance.recordingActerRecordingsAtTurns.RemoveAt(currentTurnIndex);
			if (instance.recordingActerRecordingsAtTurns.Count > currentTurnIndex - 1)
				instance.recordingActerRecordings = instance.recordingActerRecordingsAtTurns[currentTurnIndex - 1];
			if (instance.playingActerRecordingsAtTurns.Count > currentTurnIndex)
				instance.playingActerRecordingsAtTurns.RemoveAt(currentTurnIndex);
			if (instance.playingActerRecordingsAtTurns.Count > currentTurnIndex - 1)
				instance.playingActerRecordings = instance.playingActerRecordingsAtTurns[currentTurnIndex - 1];
			UpdateActerRecordings ();
			for (int i = 0; i < instance.playingActerRecordings.Count; i ++)
			{
				ActerRecording acterRecording = instance.playingActerRecordings[i];
				if (acterRecording.currentTurnPositionIndex == 0)
				{
					acterRecording.currentTurnIndex --;
					if (acterRecording.currentTurnIndex < 0)
					{
						instance.playingActerRecordings.RemoveAt(i);
						i --;
						continue;
					}
					acterRecording.currentTurnPositionIndex = acterRecording.turns[acterRecording.currentTurnIndex].positions.Length - 1;
				}
				ActerRecording.Turn currentTurn = acterRecording.turns[acterRecording.currentTurnIndex];
				for (acterRecording.currentTurnPositionIndex = acterRecording.currentTurnPositionIndex; acterRecording.currentTurnPositionIndex >= 0; acterRecording.currentTurnPositionIndex --)
				{
					Vector2Int position = currentTurn.positions[acterRecording.currentTurnPositionIndex];
					Collider2D hitCollider = Physics2D.OverlapPoint(position, GameManager.instance.whatIsSolid);
					if (hitCollider != null)
					{
						Acter _acter = hitCollider.GetComponent<Acter>();
						if (_acter == null || _acter.name != acterRecording.acterName)
							break;
					}
					acterRecording.acter.gameObject.SetActive(true);
					acterRecording.acter.trs.position = (Vector2) position;
					acterRecording.acter.trs.eulerAngles = Vector3.forward * currentTurn.rotations[acterRecording.currentTurnPositionIndex];
					Physics2D.SyncTransforms();
				}
				if (acterRecording.currentTurnPositionIndex < 0)
					acterRecording.currentTurnPositionIndex = 0;
			}
			for (int i = 0; i < recordingActerRecordings.Count; i ++)
			{
				ActerRecording acterRecording = recordingActerRecordings[i];
				if (acterRecording.turns.Count > 0)
					acterRecording.turns.RemoveAt(acterRecording.turns.Count - 1);
				if (acterRecording.turns.Count == 0)
				{
					acterRecordings.Remove(acterRecording);
					recordingActerRecordings.RemoveAt(i);
					i --;
				}
			}
			MakeNewGameState ();
			if (acterRecordingsAtTurns.Count > 0)
				acterRecordingsAtTurns.RemoveAt(acterRecordingsAtTurns.Count - 1);
			if (recordingActerRecordingsAtTurns.Count > 0)
				recordingActerRecordingsAtTurns.RemoveAt(recordingActerRecordingsAtTurns.Count - 1);
			if (playingActerRecordingsAtTurns.Count > 0)
				playingActerRecordingsAtTurns.RemoveAt(playingActerRecordingsAtTurns.Count - 1);
		}

		void HandleRestart ()
		{
			if (Keyboard.current.rKey.wasPressedThisFrame)
			{
				int currentTurnIndex = GameManager.currentTurnIndex;
				for (int i = 1; i < currentTurnIndex; i ++)
					UndoToPreviousGameState ();
				Init ();
				MakeNewGameState ();
			}
		}

		void HandleLevelSwitch ()
		{
			if (Keyboard.current.nKey.wasPressedThisFrame)
				_SceneManager.Instance.NextScene ();
			else if (Keyboard.current.bKey.wasPressedThisFrame)
				_SceneManager.Instance.PreviousScene ();
		}

		// void HandleMenu ()
		// {
		// 	bool menuInput = InputManager.MenuInput;
		// 	if (menuGo != null && menuInput && !previousMenuInput)
		// 	{
		// 		Time.timeScale = 0;
		// 		menuGo.SetActive(true);
		// 	}
		// 	previousMenuInput = menuInput;
		// }

		public static GameObject MakeNewGameState ()
		{
			if (currentTurnIndex == 1)
			{
				Sandworm[] sandworms = CurrentGameStateGo.GetComponentsInChildren<Sandworm>();
				for (int i = 0; i < sandworms.Length; i ++)
				{
					Sandworm sandworm = sandworms[i];
					sandworm.Eat ();
				}
				Pusher.instances = CurrentGameStateGo.GetComponentsInChildren<Pusher>();
				for (int i = 0; i < Pusher.instances.Length; i ++)
				{
					Pusher pusher = Pusher.instances[i];
					int entendAmount = Mathf.Min(pusher.currentExtendedAmount, pusher.maxExtendAmount);
					pusher.currentExtendedAmount = 0;
					for (int i2 = 0; i2 < entendAmount; i2 ++)
						pusher.HandlePush ();
				}
				BriarSource.instances = CurrentGameStateGo.GetComponentsInChildren<BriarSource>();
				for (int i = 0; i < BriarSource.instances.Length; i ++)
				{
					BriarSource briarSource = BriarSource.instances[i];
					if (briarSource != null)
					{
						briarSource.growthPoints = briarSource.GetGrowthPoints();
						briarSource.connectedBriarSources = new BriarSource[] { briarSource };
					}
				}
			}
			GameObject gameStateGo = Instantiate(gameStateGos[gameStateGos.Count - 1]);
			gameStateGos.Add(gameStateGo);
			gameStateGos[gameStateGos.Count - 2].SetActive(false);
			currentTurnIndex ++;
			Player.SetInstances ();
			for (int i2 = 0; i2 < Player.instances.Length; i2 ++)
			{
				Player player = Player.instances[i2];
				if (player != null)
				{
					player.decidedMoveIndicatorTrs.gameObject.SetActive(false);
					if (Physics2D.OverlapPoint(player.trs.position, LayerMask.GetMask("End")) != null)
					{
						SaveAndLoadManager.MostRecentSaveFilePath = null;
						_SceneManager.instance.LoadScene (_SceneManager.CurrentScene.buildIndex + 1);
						return gameStateGo;
					}
				}
			}
			Player.instances = gameStateGo.GetComponentsInChildren<Player>();
			Scroll.instances = gameStateGo.GetComponentsInChildren<Scroll>();
			for (int i = 0; i < Scroll.instances.Length; i ++)
			{
				Scroll scroll = Scroll.instances[i];
				if (scroll != null)
				{
					bool shouldShowScroll = false;
					for (int i2 = 0; i2 < Player.instances.Length; i2 ++)
					{
						Player player = Player.instances[i2];
						if (player != null)
						{
							Collider2D[] hitColliders = Physics2D.OverlapPointAll(player.trs.position, LayerMask.GetMask("Scroll"));
							for (int i3 = 0; i3 < hitColliders.Length; i3 ++)
							{
								Collider2D hitCollider = hitColliders[i3];
								if (hitCollider.GetComponentInParent<Scroll>() == scroll)
								{
									shouldShowScroll = true;
									break;
								}
							}
							if (shouldShowScroll)
								break;
						}
					}
					scroll.ShowOrHideContents (shouldShowScroll);
				}
			}
			diedCountAtTurns.Add(diedCount);
			UpdateActerRecordings ();
			instance.acterRecordingsAtTurns.Add(new List<ActerRecording>(instance.acterRecordings));
			instance.recordingActerRecordingsAtTurns.Add(new List<ActerRecording>(instance.recordingActerRecordings));
			instance.playingActerRecordingsAtTurns.Add(new List<ActerRecording>(instance.playingActerRecordings));
			HandleOverlappingArt ();
			return gameStateGo;
		}

		public static void UndoToPreviousGameState ()
		{
			currentTurnIndex --;
			DestroyImmediate(gameStateGos[currentTurnIndex]);
			gameStateGos.RemoveAt(currentTurnIndex);
			gameStateGos[currentTurnIndex - 1].SetActive(true);
			diedCountAtTurns.RemoveAt(currentTurnIndex);
			diedCount = diedCountAtTurns[currentTurnIndex - 1];
		}

		public static void UpdateActerRecordings ()
		{
			for (int i = 0; i < instance.acterRecordings.Count; i ++)
			{
				ActerRecording acterRecording = instance.acterRecordings[i];
				Transform acterTrs = Player.instance.trs.parent.Find(acterRecording.acterName);
				if (acterTrs != null)
					acterRecording.acter = acterTrs.GetComponent<Acter>();
				else
				{
					acterRecording.acterName = "Player";
					acterRecording.acter = Player.instance;
				}
			}
		}

		public static void HandlePushers ()
		{
			Pusher.instances = CurrentGameStateGo.GetComponentsInChildren<Pusher>();
			for (int i = 0; i < Pusher.instances.Length; i ++)
			{
				Pusher pusher = Pusher.instances[i];
				pusher.pushedThisTurn = false;
				if (pusher.currentExtendedAmount < pusher.maxExtendAmount)
					pusher.HandlePush ();
				if (pusher.currentExtendedAmount > 0)
				{
					bool shouldBreak = false;
					for (int i2 = 1; i2 < pusher.initChildCount; i2 ++)
					{
						Transform extentsPiece = pusher.trs.GetChild(i2);
						int retractCheckCount = pusher.maxExtendAmount;
						if (pusher.trs.eulerAngles.z == 0)
							retractCheckCount ++;
						for (int i3 = 0; i3 < retractCheckCount; i3 ++)
						{
							Collider2D hitCollider = Physics2D.OverlapPoint(extentsPiece.position + pusher.trs.up * i3, instance.whatIsSolid);
							if (hitCollider != null && hitCollider.GetComponent<Transform>().parent != pusher.trs)
							{
								pusher.pushedThisTurn = true;
								shouldBreak = true;
								break;
							}
						}
						if (shouldBreak)
							break;
					}
					if (!pusher.pushedThisTurn)
						pusher.Retract ();
				}
			}
		}

		public static void HandleMoveTiles ()
		{
			MoveTile.instances = CurrentGameStateGo.GetComponentsInChildren<MoveTile>();
			for (int i = 0; i < MoveTile.instances.Length; i ++)
			{
				MoveTile moveTile = MoveTile.instances[i];
				moveTile.Act ();
			}
		}

		public static bool AddStackEntry ()
		{
			stackEntryCount ++;
			if (stackEntryCount > MAX_STACK_ENTRY_COUNT)
			{
				previousUpdatables = new IUpdatable[updatables.Length];
				updatables.CopyTo(previousUpdatables, 0);
				updatables = new IUpdatable[0];
				instance.enabled = false;
				return false;
			}
			return true;
		}

		static void HandleOverlappingArt ()
		{
			SpriteRenderer[] spriteRenderers = CurrentGameStateGo.GetComponentsInChildren<SpriteRenderer>();
			for (int i = 0; i < spriteRenderers.Length; i ++)
			{
				SpriteRenderer spriteRenderer = spriteRenderers[i];
				Transform spriteRendererTrs = spriteRenderer.GetComponent<Transform>();
				bool sharesPositionWithOtherSpriteRenderer = false;
				int i2 = 0;
				if (i < spriteRenderers.Length - 1)
					i2 = i + 1;
				for (i2 = i2; i2 < spriteRenderers.Length; i2 ++)
				{
					if (i == i2)
						continue;
					SpriteRenderer spriteRenderer2 = spriteRenderers[i2];
					Transform spriteRenderer2Trs = spriteRenderer2.GetComponent<Transform>();
					if (spriteRendererTrs.position == spriteRenderer2Trs.position && (spriteRenderer.color != spriteRenderer2.color || spriteRenderer.sharedMaterial != spriteRenderer2.sharedMaterial))
					{
						sharesPositionWithOtherSpriteRenderer = true;
						if (!namesOfAlteredSpriteRenderers.Contains(spriteRenderer.name))
						{
							spriteRenderer.material.SetColor("_tint", spriteRenderer.material.GetColor("_tint").DivideAlpha(2));
							namesOfAlteredSpriteRenderers.Add(spriteRenderer.name);
						}
						if (!namesOfAlteredSpriteRenderers.Contains(spriteRenderer2.name))
						{
							spriteRenderer2.material.SetColor("_tint", spriteRenderer2.material.GetColor("_tint").DivideAlpha(2));
							namesOfAlteredSpriteRenderers.Add(spriteRenderer2.name);
						}
					}
					break;
				}
				if (!sharesPositionWithOtherSpriteRenderer && namesOfAlteredSpriteRenderers.Contains(spriteRenderer.name))
				{
					spriteRenderer.material.SetColor("_tint", spriteRenderer.material.GetColor("_tint").MultiplyAlpha(2));
					namesOfAlteredSpriteRenderers.Remove(spriteRenderer.name);
				}
			}
			LineRenderer[] lineRenderers = CurrentGameStateGo.GetComponentsInChildren<LineRenderer>();
			for (int i = 0; i < lineRenderers.Length; i ++)
			{
				LineRenderer lineRenderer = lineRenderers[i];
				if (!lineRenderer.enabled)
					continue;
				LineSegment2D lineSegment = new LineSegment2D(lineRenderer.GetPosition(0), lineRenderer.GetPosition(1));
				if (!lineRenderer.useWorldSpace)
				{
					Transform lineRendererTrs = lineRenderer.GetComponent<Transform>();
					lineSegment = new LineSegment2D(lineRendererTrs.TransformPoint(lineSegment.start), lineRendererTrs.TransformPoint(lineSegment.end));
				}
				bool sharesPositionsWithOtherLineRenderer = false;
				int i2 = 0;
				if (i < lineRenderers.Length - 1)
					i2 = i + 1;
				for (i2 = i2; i2 < lineRenderers.Length; i2 ++)
				{
					if (i == i2)
						continue;
					LineRenderer lineRenderer2 = lineRenderers[i2];
					if (!lineRenderer2.enabled)
						continue;
					LineSegment2D lineSegment2 = new LineSegment2D(lineRenderer2.GetPosition(0), lineRenderer2.GetPosition(1));
					if (!lineRenderer2.useWorldSpace)
					{
						Transform lineRenderer2Trs = lineRenderer2.GetComponent<Transform>();
						lineSegment2 = new LineSegment2D(lineRenderer2Trs.TransformPoint(lineSegment2.start), lineRenderer2Trs.TransformPoint(lineSegment2.end));
					}
					if (lineSegment.Encapsulates(lineSegment2) || lineSegment2.Encapsulates(lineSegment))
					{
						Vector2 offset = lineSegment.GetPerpendicular().GetDirection();
						lineSegment = lineSegment.Move(offset * lineRenderer.startWidth / 2);
						sharesPositionsWithOtherLineRenderer = true;
						if (!namesOfAlteredLineRenderers.Contains(lineRenderer.name))
						{
							bool previousUsingWorldSpace = lineRenderer.useWorldSpace;
							lineRenderer.SetUseWorldSpace (true);
							lineRenderer.SetPositions(new Vector3[] { lineSegment.start, lineSegment.end });
							lineRenderer.SetUseWorldSpace (previousUsingWorldSpace);
							namesOfAlteredLineRenderers.Add(lineRenderer.name);
						}
						if (!namesOfAlteredLineRenderers.Contains(lineRenderer2.name))
						{
							lineSegment2 = lineSegment2.Move(-offset * lineRenderer2.startWidth / 2);
							bool previousUsingWorldSpace = lineRenderer2.useWorldSpace;
							lineRenderer2.SetUseWorldSpace (true);
							lineRenderer2.SetPositions(new Vector3[] { lineSegment2.start, lineSegment2.end });
							lineRenderer2.SetUseWorldSpace (previousUsingWorldSpace);
							namesOfAlteredLineRenderers.Add(lineRenderer2.name);
						}
					}
					break;
				}
				if (!sharesPositionsWithOtherLineRenderer && namesOfAlteredLineRenderers.Contains(lineRenderer.name))
				{
					Vector2 offset = lineSegment.GetPerpendicular().GetDirection();
					lineSegment = lineSegment.Move(-offset * lineRenderer.startWidth / 2);
					bool previousUsingWorldSpace = lineRenderer.useWorldSpace;
					lineRenderer.SetUseWorldSpace (true);
					lineRenderer.SetPositions(new Vector3[] { lineSegment.start, lineSegment.end });
					lineRenderer.SetUseWorldSpace (previousUsingWorldSpace);
					namesOfAlteredLineRenderers.Remove(lineRenderer.name);
				}
			}
		}

		public void Quit ()
		{
			Application.Quit();
#if UNITY_EDITOR
			EditorApplication.isPlaying = false;
#endif
		}

		bool OnWantToQuit ()
		{
			// isQuitting = true;
			// PlayerPrefs.DeleteAll();
			SaveAndLoadManager.instance.Save (SaveAndLoadManager.DefaultSaveFilePath);
			Application.wantsToQuit -= OnWantToQuit;
			return true;
		}

		public void ToggleGameObject (GameObject go)
		{
			go.SetActive(!go.activeSelf);
		}

		public void DestroyChildren (Transform trs)
		{
			for (int i = 0; i < trs.childCount; i ++)
				Destroy(trs.GetChild(i).gameObject);
		}

		public void DestroyChildrenImmediate (Transform trs)
		{
			for (int i = 0; i < trs.childCount; i ++)
				DestroyImmediate(trs.GetChild(0).gameObject);
		}

		public void SetTimeScale (float timeScale)
		{
			Time.timeScale = timeScale;
		}

		public static void Log (object obj)
		{
			print(obj);
		}
		
#if UNITY_EDITOR
		public static void DestroyOnNextEditorUpdate (Object obj)
		{
			EditorApplication.update += () => { if (obj == null) return; DestroyObject (obj); };
		}

		static void DestroyObject (Object obj)
		{
			if (obj == null)
				return;
			EditorApplication.update -= () => { DestroyObject (obj); };
			DestroyImmediate(obj);
		}

		public static void DoOnNextEditorUpdate (Action action)
		{
			EditorApplication.update += () => { Do (action); };
		}

		static void Do (Action action)
		{
			EditorApplication.update -= () => { Do (action); };
			action ();
		}
#endif
		
		// public static bool ModifierExistsAndIsActive (string name)
		// {
		// 	GameModifier gameModifier;
		// 	if (gameModifierDict.TryGetValue(name, out gameModifier))
		// 		return gameModifier.isActive;
		// 	else
		// 		return false;
		// }

		// public static bool ModifierIsActive (string name)
		// {
		// 	return gameModifierDict[name].isActive;
		// }

		// public static bool ModifierExists (string name)
		// {
		// 	return gameModifierDict.ContainsKey(name);
		// }

		// [Serializable]
		// public class GameModifier
		// {
		// 	public string name;
		// 	public bool isActive;
		// }

		[Serializable]
		public class GameState
		{
			public Asset.Data[] assetsDatas = new Asset.Data[0];

			public GameState (Asset.Data[] assetsDatas)
			{
				this.assetsDatas = assetsDatas;
			}
		}
	}
}