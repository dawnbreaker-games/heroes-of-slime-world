using System;
using Extensions;
using UnityEngine;

namespace HeroesOfSlimeWorld
{
	public class BriarGrowth : Acter
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public override void ApplyGravity ()
		{
		}

		public override void Death ()
		{
		}
		
		public override void InitData ()
		{
			if (data == null)
				data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Acter.Data
		{
			public override object MakeAsset ()
			{
				BriarGrowth briarGrowth = Instantiate(GameManager.instance.briarGrowthPrefab, Player.instance.trs.parent);
				Apply (briarGrowth);
				return briarGrowth;
			}

			public override void Apply (Asset asset)
			{
				if (asset.data == null)
					asset.data = this;
				base.Apply (asset);
			}
		}
	}
}