using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace HeroesOfSlimeWorld
{
	public class Bomb : Box
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Hazard hazardPrefab;
		public Fire firePrefab;
		public LayerMask whatBlocksExplosion;
		public Transform[] explosionPaths = new Transform[0];

		public override bool TryToMoveTo (Vector2Int position)
		{
			if (!GameManager.AddStackEntry())
				return false;
			if (Physics2D.OverlapPoint(position, whatICrashInto) == null)
			{
				SetPosition (position);
				return true;
			}
			else if (fellUnitsThisTurn > 0)
				Death ();
			return false;
		}

		public override void OnBeforeDeath ()
		{
			if (!GameManager.AddStackEntry())
				return;
			base.OnBeforeDeath ();
			List<FragileWallTile> fragileWallTiles = new List<FragileWallTile>();
			for (int i = 0; i < explosionPaths.Length; i ++)
			{
				Transform explosionPath = explosionPaths[i];
				Vector2 offset = Vector2.zero;
				Vector2 previousPosition = trs.position;
				for (int i2 = 0; i2 < explosionPath.childCount; i2 ++)
				{
					Transform explosionPositionTrs = explosionPath.GetChild(i2);
					Vector2 moveDirection = (Vector2) explosionPositionTrs.position - previousPosition;
					Collider2D hitFragileWallCollider;
					if (moveDirection.x != 0 && moveDirection.y != 0)
					{
						hitFragileWallCollider = Physics2D.OverlapPoint(previousPosition + offset + new Vector2(moveDirection.x, 0), LayerMask.GetMask("Fragile Wall"));
						if (hitFragileWallCollider != null)
							fragileWallTiles.Add(hitFragileWallCollider.GetComponent<FragileWallTile>());
						hitFragileWallCollider = Physics2D.OverlapPoint(previousPosition + offset + new Vector2(0, moveDirection.y), LayerMask.GetMask("Fragile Wall"));
						if (hitFragileWallCollider != null)
							fragileWallTiles.Add(hitFragileWallCollider.GetComponent<FragileWallTile>());
						if (Physics2D.OverlapPoint(previousPosition + offset + new Vector2(moveDirection.x, 0), whatBlocksExplosion) != null && Physics2D.OverlapPoint(previousPosition + offset + new Vector2(0, moveDirection.y), whatBlocksExplosion) != null)
						{
							previousPosition += moveDirection;
							break;
						}
					}
					previousPosition += moveDirection;
					hitFragileWallCollider = Physics2D.OverlapPoint((Vector2) explosionPositionTrs.position + offset, LayerMask.GetMask("Fragile Wall"));
					if (hitFragileWallCollider != null)
						fragileWallTiles.Add(hitFragileWallCollider.GetComponent<FragileWallTile>());
					Collider2D[] hitPortalColliders = Physics2D.OverlapPointAll((Vector2) explosionPositionTrs.position + offset, LayerMask.GetMask("Portal"));
					for (int i3 = 0; i3 < hitPortalColliders.Length; i3 ++)
					{
						Collider2D hitPortalCollider = hitPortalColliders[i3];
						Portal portal = hitPortalCollider.GetComponent<Portal>();
						if (Physics2D.OverlapPoint(portal.teleportTo.trs.position, whatBlocksExplosion) == null)
						{
							Hazard hazard = Instantiate(hazardPrefab, (Vector2) explosionPositionTrs.position + offset, Quaternion.identity);
							hazard.HandleKill ();
							DestroyImmediate(hazard.gameObject);
							Fire fire = Instantiate(firePrefab, (Vector2) explosionPositionTrs.position + offset, Quaternion.identity);
							Destroy(fire.gameObject, fire.animationEntry.GetClipLength());
							offset = portal.teleportTo.trs.position - explosionPositionTrs.position;
						}
					}
					if (Physics2D.OverlapPoint((Vector2) explosionPositionTrs.position + offset, whatBlocksExplosion) == null)
					{
						Hazard hazard = Instantiate(hazardPrefab, (Vector2) explosionPositionTrs.position + offset, Quaternion.identity);
						hazard.HandleKill ();
						DestroyImmediate(hazard.gameObject);
						Fire fire = Instantiate(firePrefab, (Vector2) explosionPositionTrs.position + offset, Quaternion.identity);
						Destroy(fire.gameObject, fire.animationEntry.GetClipLength());
					}
					else
						break;
				}
			}
			for (int i = 0; i < fragileWallTiles.Count; i ++)
			{
				FragileWallTile fragileWallTile = fragileWallTiles[i];
				if (fragileWallTile != null)
					fragileWallTile.Death ();
			}
			Collider2D hitBriarCollider = Physics2D.OverlapPoint(trs.position, LayerMask.GetMask("Briar", "Briar Source"));
			if (hitBriarCollider != null)
				DestroyImmediate(hitBriarCollider.gameObject);
		}
		
		public override void InitData ()
		{
			if (data == null)
				data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Box.Data
		{
			public override object MakeAsset ()
			{
				Bomb bomb = Instantiate(GameManager.instance.bombPrefab, Player.instance.trs.parent);
				Apply (bomb);
				return bomb;
			}

			public override void Apply (Asset asset)
			{
				if (asset.data == null)
					asset.data = this;
				base.Apply (asset);
			}
		}
	}
}