using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace HeroesOfSlimeWorld
{
	public class Box : Acter
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		
		public override bool TryToMoveTo (Vector2Int position)
		{
			if (!GameManager.AddStackEntry())
				return false;
			if ((trs.position.ToVec2Int() - position).sqrMagnitude > 1)
				return false;
			bool output = Physics2D.OverlapPoint(position, whatICrashInto) == null;
			if (output)
				SetPosition (position);
			else
				HandleRecordingAndPlaybackAt (position);
			return output;
		}

		public override void Death ()
		{
			if (!GameManager.AddStackEntry())
				return;
			if (isDead)
				return;
			isDead = true;
			OnBeforeDeath ();
			DestroyImmediate(gameObject);
		}
		
		public override void InitData ()
		{
			if (data == null)
				data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Acter.Data
		{
			public override object MakeAsset ()
			{
				Box box = Instantiate(GameManager.instance.boxPrefab, Player.instance.trs.parent);
				Apply (box);
				return box;
			}

			public override void Apply (Asset asset)
			{
				if (asset.data == null)
					asset.data = this;
				base.Apply (asset);
			}
		}
	}
}