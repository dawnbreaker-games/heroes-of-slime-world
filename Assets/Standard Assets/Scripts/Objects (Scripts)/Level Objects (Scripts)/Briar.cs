using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace HeroesOfSlimeWorld
{
	public class Briar : Acter
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		[HideInInspector]
		public BriarSource[] sources = new BriarSource[0];

		public override void ApplyGravity ()
		{
		}

		public override void Death ()
		{
			if (!GameManager.AddStackEntry())
				return;
			base.Death ();
			for (int i = 0; i < sources.Length; i ++)
			{
				BriarSource briarSource = sources[i];
				briarSource.connectedBriars = briarSource.connectedBriars.Remove(this);
				briarSource.outerConnectedBriars = briarSource.outerConnectedBriars.Remove(this);
			}
		}

		public void OnDisconnectFromSources ()
		{
			base.ApplyGravity ();
		}
		
		public override void InitData ()
		{
			if (data == null)
				data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Acter.Data
		{
			public override object MakeAsset ()
			{
				Briar briar = Instantiate(GameManager.instance.briarPrefab, Player.instance.trs.parent);
				Apply (briar);
				return briar;
			}

			public override void Apply (Asset asset)
			{
				if (asset.data == null)
					asset.data = this;
				base.Apply (asset);
			}
		}
	}
}