using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace HeroesOfSlimeWorld
{
	public class BriarSource : Acter
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		[HideInInspector]
		public Briar[] connectedBriars = new Briar[0];
		[HideInInspector]
		public BriarSource[] connectedBriarSources = new BriarSource[0];
		[HideInInspector]
		public List<BriarGrowth> briarGrowths = new List<BriarGrowth>();
		[HideInInspector]
		public Briar[] outerConnectedBriars = new Briar[0];
		[HideInInspector]
		public Vector2Int[] growthPoints = new Vector2Int[0];
		public static BriarSource[] instances = new BriarSource[0];
		
		public override void Act ()
		{
			if (!GameManager.AddStackEntry())
				return;
			if (Player.instance.isDead)
				return;
			if (briarGrowths.Count == growthPoints.Length)
			{
				List<Briar> newBriars = new List<Briar>();
				for (int i = 0; i < briarGrowths.Count; i ++)
				{
					BriarGrowth briarGrowth = briarGrowths[i];
					Collider2D hitCollider = Physics2D.OverlapPoint(briarGrowth.trs.position, whatICrashInto);
					if (hitCollider == null)
					{
						instances = GameManager.CurrentGameStateGo.GetComponentsInChildren<BriarSource>();
						for (int i2 = 0; i2 < instances.Length; i2 ++)
						{
							BriarSource briarSource = instances[i2];
							if (!connectedBriarSources.Contains(briarSource))
							{
								for (int i3 = 0; i3 < briarSource.growthPoints.Length; i3 ++)
								{
									Vector2Int growthPoint = briarSource.growthPoints[i3];
									if (growthPoint == briarGrowth.trs.position.ToVec2Int())
									{
										connectedBriarSources = connectedBriarSources.Add(briarSource);
										connectedBriars = connectedBriars.MergeAndRemoveCopies(new List<Briar>(briarSource.connectedBriars)).ToArray();
										briarGrowths = briarGrowths.ToArray().MergeAndRemoveCopies(briarSource.briarGrowths);
									}
								}
							}
						}
						Briar connectedBriar = Instantiate(GameManager.instance.briarPrefab, briarGrowth.trs.position, Quaternion.identity, Player.instance.trs.parent);
						connectedBriar.sources = connectedBriarSources;
						connectedBriars = connectedBriars.Add(connectedBriar);
						newBriars.Add(connectedBriar);
						Acter[] teleportedActers = connectedBriar.HandleTeleportAt(connectedBriar.trs.position.ToVec2Int());
						for (int i2 = 0; i2 < teleportedActers.Length; i2 ++)
						{
							Acter teleportedActer = teleportedActers[i2];
							Briar teleportedBriar = (Briar) teleportedActer;
							teleportedBriar.sources = new BriarSource[0];
							teleportedBriar.OnDisconnectFromSources ();
						}
					}
					DestroyImmediate(briarGrowth.gameObject);
					briarGrowths.RemoveAt(i);
					i --;
				}
				outerConnectedBriars = newBriars.ToArray();
				growthPoints = GetGrowthPoints();
			}
			else
			{
				BriarGrowth briarGrowth = Instantiate(GameManager.instance.briarGrowthPrefab, growthPoints[briarGrowths.Count].ToVec3(), Quaternion.identity, Player.instance.trs.parent);
				briarGrowths.Add(briarGrowth);
			}
			for (int i = 0; i < connectedBriars.Length; i ++)
			{
				Briar conenctedBriar = connectedBriars[i];
				if (conenctedBriar != null)
					conenctedBriar.hazard.HandleKill ();
			}
			hazard.HandleKill ();
			for (int i = 0; i < connectedBriarSources.Length; i ++)
			{
				BriarSource briarSource = connectedBriarSources[i];
				briarSource.connectedBriars = connectedBriars;
				briarSource.connectedBriarSources = connectedBriarSources;
				briarSource.briarGrowths = briarGrowths;
				briarSource.outerConnectedBriars = outerConnectedBriars;
				briarSource.growthPoints = growthPoints;
			}
		}

		public override void Death ()
		{
			if (!GameManager.AddStackEntry())
				return;
			base.Death ();
			for (int i = 0; i < connectedBriars.Length; i ++)
			{
				Briar connectedBriar = connectedBriars[i];
				connectedBriar.sources = connectedBriar.sources.Remove(this);
			}
			connectedBriarSources = connectedBriarSources.Remove(this);
		}

		public override void ApplyGravity ()
		{
		}

		public override void OnBeforeDeath ()
		{
			if (!GameManager.AddStackEntry())
				return;
			base.OnBeforeDeath ();
			for (int i = 0; i < connectedBriars.Length; i ++)
			{
				Briar briar = connectedBriars[i];
				briar.sources = briar.sources.Remove(this);
				if (briar.sources.Length == 0)
					briar.OnDisconnectFromSources ();
			}
			for (int i = 0; i < connectedBriarSources.Length; i ++)
			{
				BriarSource briarSource = connectedBriarSources[i];
				briarSource.connectedBriarSources = briarSource.connectedBriarSources.Remove(this);
			}
		}

		public Vector2Int[] GetGrowthPoints ()
		{
			List<Vector2Int> output = new List<Vector2Int>();
			Vector2Int position = trs.position.ToVec2Int();
			HandleGrowthPoint (position + Vector2Int.up, ref output);
			HandleGrowthPoint (position + Vector2Int.down, ref output);
			HandleGrowthPoint (position + Vector2Int.left, ref output);
			HandleGrowthPoint (position + Vector2Int.right, ref output);
			for (int i = 0; i < outerConnectedBriars.Length; i ++)
			{
				Briar outerConnectedBriar = outerConnectedBriars[i];
				position = outerConnectedBriar.trs.position.ToVec2Int();
				HandleGrowthPoint (position + Vector2Int.up, ref output);
				HandleGrowthPoint (position + Vector2Int.down, ref output);
				HandleGrowthPoint (position + Vector2Int.left, ref output);
				HandleGrowthPoint (position + Vector2Int.right, ref output);
			}
			for (int i = 0; i < connectedBriarSources.Length; i ++)
			{
				BriarSource connectedBriarSource = connectedBriarSources[i];
				position = connectedBriarSource.trs.position.ToVec2Int();
				HandleGrowthPoint (position + Vector2Int.up, ref output);
				HandleGrowthPoint (position + Vector2Int.down, ref output);
				HandleGrowthPoint (position + Vector2Int.left, ref output);
				HandleGrowthPoint (position + Vector2Int.right, ref output);
			}
			return output.ToArray();
		}

		void HandleGrowthPoint (Vector2Int position, ref List<Vector2Int> growthPoints)
		{
			if (growthPoints.Contains(position))
				return;
			if (Physics2D.OverlapPoint(position, whatICrashInto) != null)
				growthPoints.Remove(position);
			else
				growthPoints.Add(position);
		}
		
		public override void InitData ()
		{
			if (data == null)
				data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Acter.Data
		{
			public override object MakeAsset ()
			{
				BriarSource briarSource = Instantiate(GameManager.instance.briarSourcePrefab, Player.instance.trs.parent);
				Apply (briarSource);
				return briarSource;
			}

			public override void Apply (Asset asset)
			{
				if (asset.data == null)
					asset.data = this;
				base.Apply (asset);
			}
		}
	}
}