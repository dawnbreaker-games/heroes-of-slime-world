using TMPro;
using System;
using Extensions;
using System.Collections.Generic;

namespace HeroesOfSlimeWorld
{
	public class DeathGate : Gate
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public uint killsRequired;
		public TMP_Text text;
		public static List<DeathGate> instances = new List<DeathGate>();
		
#if UNITY_EDITOR
		public override void OnValidate ()
		{
			base.OnValidate ();
			text.text = "" + killsRequired;
		}
#endif

		public override void OnEnable ()
		{
			instances.Add(this);
			text.text = "" + killsRequired;
		}

		public override void OnDisable ()
		{
			instances.Remove(this);
		}

		public override void HandleIsOpen ()
		{
			text.text = "" + (killsRequired - GameManager.diedCount);
			if (GameManager.diedCount == killsRequired)
			{
				Toggle ();
				text.enabled = false;
			}
		}

		public override void Open ()
		{
			base.Open ();
			text.color = text.color.SetAlpha(openColor.a);
		}

		public override void Close ()
		{
			base.Close ();
			text.color = text.color.SetAlpha(closedColor.a);
		}

		public override void InitData ()
		{
			if (data == null)
				data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetKillsRequiredOfData ();
		}

		void SetKillsRequiredOfData ()
		{
			_Data.killsRequired = killsRequired;
		}

		void SetKillsRequiredFromData ()
		{
			killsRequired = _Data.killsRequired;
			text.text = "" + killsRequired;
		}
		
		[Serializable]
		public class Data : Gate.Data
		{
			public uint killsRequired;

			public override object MakeAsset ()
			{
				DeathGate deathGate = Instantiate(GameManager.instance.deathGatePrefab, Player.instance.trs.parent);
				Apply (deathGate);
				return deathGate;
			}

			public override void Apply (Asset asset)
			{
				if (asset.data == null)
					asset.data = this;
				base.Apply (asset);
				DeathGate deathGate = (DeathGate) asset;
				deathGate.SetKillsRequiredFromData ();
			}
		}
	}
}