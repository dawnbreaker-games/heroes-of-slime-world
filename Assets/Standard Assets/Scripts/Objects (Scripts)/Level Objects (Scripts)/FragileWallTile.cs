using System;
using Extensions;
using UnityEngine;

namespace HeroesOfSlimeWorld
{
	public class FragileWallTile : WallTile
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public override void InitData ()
		{
			if (data == null)
				data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}
		
		[Serializable]
		public class Data : WallTile.Data
		{
			public bool used;

			public override object MakeAsset ()
			{
				FragileWallTile fragileWallTile = Instantiate(GameManager.instance.fragileWallTilePrefab, Player.instance.trs.parent);
				Apply (fragileWallTile);
				return fragileWallTile;
			}

			public override void Apply (Asset asset)
			{
				if (asset.data == null)
					asset.data = this;
				base.Apply (asset);
			}
		}
	}
}