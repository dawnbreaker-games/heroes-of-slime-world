using System;
using Extensions;
using UnityEngine;

namespace HeroesOfSlimeWorld
{
	public class Gel : Asset
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public LineRenderer lineRenderer;
		[HideInInspector]
		public Acter stuckTo;
		[HideInInspector]
		public Acter stuckTo2;

		public void OnValidate ()
		{
			if (lineRenderer == null || stuckTo == null || stuckTo2 == null)
				return;
			LineSegment2D lineSegment = new LineSegment2D(stuckTo.trs.position, stuckTo2.trs.position).GetPerpendicular();
			lineRenderer.SetPositions(new Vector3[] { lineSegment.start, lineSegment.end });
			if (!stuckTo.stuckToGels.Contains(this))
				stuckTo.stuckToGels = stuckTo.stuckToGels.Add(this);
			if (!stuckTo2.stuckToGels.Contains(this))
				stuckTo2.stuckToGels = stuckTo2.stuckToGels.Add(this);
		}

		public override void InitData ()
		{
			if (data == null)
				data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetStuckToOfData ();
			SetStuckTo2OfData ();
		}

		void SetStuckToOfData ()
		{
			_Data.stuckToData = stuckTo._Data;
		}

		void SetStuckToFromData ()
		{
			stuckTo = Asset.Get<Acter>(_Data.stuckToData);
		}

		void SetStuckTo2OfData ()
		{
			_Data.stuckTo2Data = stuckTo2._Data;
		}

		void SetStuckTo2FromData ()
		{
			stuckTo2 = Asset.Get<Acter>(_Data.stuckTo2Data);
			OnValidate ();
		}
		
		[Serializable]
		public class Data : Asset.Data
		{
			public Acter.Data stuckToData;
			public Acter.Data stuckTo2Data;

			public override object MakeAsset ()
			{
				Gel gel = Instantiate(GameManager.instance.gelPrefab, Player.instance.trs.parent);
				Apply (gel);
				return gel;
			}

			public override void Apply (Asset asset)
			{
				if (asset.data == null)
					asset.data = this;
				base.Apply (asset);
				Gel gel = (Gel) asset;
				gel.SetStuckToFromData ();
				gel.SetStuckTo2FromData ();
			}
		}
	}
}