using System;
using Extensions;
using UnityEngine;

namespace HeroesOfSlimeWorld
{
	public class MoveTile : WallTile
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Transform rotateToTrsOnCantMove;
		public Action onMove;
		public static MoveTile[] instances = new MoveTile[0];

		public override bool TryToMoveTo (Vector2Int position)
		{
			bool output = false;
			Collider2D hitCollider = Physics2D.OverlapPoint(position, whatICrashInto);
			if (hitCollider == null)
				output = true;
			if (output)
				SetPosition (position);
			else
				HandleRecordingAndPlaybackAt (position);
			return output;
		}

		public override void Act ()
		{
			if (!TryToMoveTo((trs.position + trs.up).ToVec2Int()))
			{
				trs.up = rotateToTrsOnCantMove.up;
				cellsEnteredThisTurn.Add(trs.position.ToVec2Int());
				rotationsThisTurn.Add((uint) trs.eulerAngles.z);
			}
			else if (onMove != null)
				onMove ();
			base.Act ();
		}
		
		public override void InitData ()
		{
			if (data == null)
				data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : WallTile.Data
		{
			public override object MakeAsset ()
			{
				MoveTile moveTile = Instantiate(GameManager.instance.moveTilePrefab, Player.instance.trs.parent);
				Apply (moveTile);
				return moveTile;
			}

			public override void Apply (Asset asset)
			{
				if (asset.data == null)
					asset.data = this;
				base.Apply (asset);
			}
		}
	}
}