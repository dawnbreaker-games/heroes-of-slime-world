using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace HeroesOfSlimeWorld
{
	public class PlaybackTile : WallTile
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public SpriteRenderer spriteRenderer;
		public Color usedColor;
		[HideInInspector]
		[SaveAndLoadValue]
		public bool used;
		static Dictionary<string, uint> lastActerNamesSuffixesDict = new Dictionary<string, uint>();

		public void StartPlaying ()
		{
			for (int i = 0; i < GameManager.instance.acterRecordings.Count; i ++)
			{
				ActerRecording acterRecording = GameManager.instance.acterRecordings[i];
				if (GameManager.instance.playingActerRecordings.Contains(acterRecording))
				{
					acterRecording = new ActerRecording(acterRecording);
					acterRecording.acterName = acterRecording.acterName.Remove(acterRecording.acterName.IndexOf("(Clone)"));
					acterRecording.acter = GameObject.Find(acterRecording.acterName).GetComponent<Acter>();
				}
				acterRecording.currentTurnIndex = 0;
				acterRecording.currentTurnPositionIndex = 0;
				StartPlaying (acterRecording);
			}
			spriteRenderer.color = usedColor;
			used = true;
		}

		void StartPlaying (ActerRecording acterRecording)
		{
			acterRecording.turns.Add(new ActerRecording.Turn(acterRecording.acter.cellsEnteredThisTurn.ToArray(), acterRecording.acter.rotationsThisTurn.ToArray()));
			GameManager.instance.recordingActerRecordings.Remove(acterRecording);
			Player player = Player.instance;
			uint acterNameSuffix = 0;
			if (lastActerNamesSuffixesDict.TryGetValue(acterRecording.acterName + "(Clone)", out acterNameSuffix))
				acterNameSuffix ++;
			lastActerNamesSuffixesDict[acterRecording.acterName + "(Clone)"] = acterNameSuffix;
			Acter acter = GameObject.Find(acterRecording.acterName).GetComponent<Acter>();
			acterRecording.acter = Instantiate(acter, acterRecording.turns[0].positions[0].ToVec3(), Quaternion.Euler(Vector3.forward * acterRecording.turns[0].rotations[0]), Player.instance.trs.parent);
			acterRecording.acter.name += acterNameSuffix;
			acterRecording.acterName = acterRecording.acter.name;
			acterRecording.acter.enabled = false;
			Player.instance = player;
			acterRecording.acter.gameObject.SetActive(false);
			acterRecording.acter.gameObject.layer = LayerMask.NameToLayer("Recording");
			CameraScript cameraScript = acterRecording.acter.GetComponentInChildren<CameraScript>();
			player = acterRecording.acter as Player;
			if (player != null)
				GameManager.instance.DestroyChildrenImmediate (player.trs);
			GameManager.instance.playingActerRecordings.Add(acterRecording);
		}

		public override void InitData ()
		{
			if (data == null)
				data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetUsedOfData ();
		}

		void SetUsedOfData ()
		{
			_Data.used = used;
		}

		void SetUsedFromData ()
		{
			used = _Data.used;
			if (used)
				spriteRenderer.color = usedColor;
		}
		
		[Serializable]
		public class Data : WallTile.Data
		{
			public bool used;

			public override object MakeAsset ()
			{
				PlaybackTile playbackTile = Instantiate(GameManager.instance.playbackTilePrefab, Player.instance.trs.parent);
				Apply (playbackTile);
				return playbackTile;
			}

			public override void Apply (Asset asset)
			{
				if (asset.data == null)
					asset.data = this;
				base.Apply (asset);
				PlaybackTile playbackTile = (PlaybackTile) asset;
				playbackTile.SetUsedFromData ();
			}
		}
	}
}