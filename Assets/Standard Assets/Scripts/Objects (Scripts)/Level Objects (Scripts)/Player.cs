using System;
using Extensions;
using UnityEngine;
using UnityEngine.InputSystem;
using System.Collections.Generic;

namespace HeroesOfSlimeWorld
{
	public class Player : Acter, IUpdatable
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public GameObject enabledIndicatorGo;
		public Transform decidedMoveIndicatorTrs;
		public bool autoSwitchEnabledOnDecidedMove;
		public Vector2Int? decidedMove;
		public static Player instance;
		public static Player Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<Player>();
				return instance;
			}
		}
		public static Player[] instances = new Player[0];
		public static uint turnIDied;
		static uint timesSwitchedEnabledThisTurn;

		void Awake ()
		{
			if (instance == null)
				timesSwitchedEnabledThisTurn = 0;
			else if (instance != this && instance.trs.parent == trs.parent && decidedMoveIndicatorTrs != null)
				decidedMoveIndicatorTrs.gameObject.SetActive(false);
		}

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.OnEnable ();
			if (GameManager.gameStateGos.Count > 0)
				SetInstances ();
			else
				instances = FindObjectsOfType<Player>();
			if (instances.Length > 1)
				enabledIndicatorGo.SetActive(true);
			instance = this;
			GetComponentInChildren<CameraScript>(true).gameObject.SetActive(true);
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void DoUpdate ()
		{
			HandleSwitchEnabled ();
			HandleAct ();
			isDead = false;
		}

		public override void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.OnDisable ();
			enabledIndicatorGo.SetActive(false);
			GetComponentInChildren<CameraScript>(true).gameObject.SetActive(false);
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

		public override bool TryToMoveTo (Vector2Int position)
		{
			if (enabled)
				return base.TryToMoveTo(position);
			else
				return false;
		}

		void HandleAct ()
		{
			if (Keyboard.current.aKey.wasPressedThisFrame || Keyboard.current.leftArrowKey.wasPressedThisFrame)
				DecideMove (Vector2Int.left);
			else if (Keyboard.current.dKey.wasPressedThisFrame || Keyboard.current.rightArrowKey.wasPressedThisFrame)
				DecideMove (Vector2Int.right);
			else if (Keyboard.current.wKey.wasPressedThisFrame || Keyboard.current.upArrowKey.wasPressedThisFrame)
				DecideMove (Vector2Int.up);
			else if (Keyboard.current.sKey.wasPressedThisFrame || Keyboard.current.downArrowKey.wasPressedThisFrame)
				DecideMove (Vector2Int.zero);
			else
				return;
			if (Keyboard.current.ctrlKey.isPressed)
			{
				SetInstances ();
				for (int i = 0; i < instances.Length; i ++)
				{
					Player player = instances[i];
					if (player != this)
						player.DecideMove (Vector2Int.zero);
				}
			}
			else if (!AllPlayersDecidedMoves())
				return;
			OnBeforeAct ();
			SetInstances ();
			for (int i = 0; i < instances.Length; i ++)
			{
				Player player = instances[i];
				if (player.decidedMove == Vector2Int.up)
				{
					player.cellsEnteredThisTurn.Add(player.trs.position.ToVec2Int());
					player.rotationsThisTurn.Add((uint) player.trs.eulerAngles.z);
					Acter[] newActers = player.HandleTeleportAt(player.trs.position.ToVec2Int() + Vector2Int.up);
					for (int i2 = 0; i2 < newActers.Length; i2 ++)
					{
						Player player2 = (Player) newActers[i2];
						if (player2 != instance)
							player2.enabled = false;
					}
					player.HandleRecordingAndPlaybackAt (player.trs.position.ToVec2Int() + Vector2Int.up);
				}
				else if (player.decidedMove != null && player.decidedMove != Vector2Int.zero)
					player.TryToMoveTo (player.trs.position.ToVec2Int() + (Vector2Int) player.decidedMove);
				SetInstances ();
			}
			Act ();
		}

		void DecideMove (Vector2Int move)
		{
			if (!GameManager.AddStackEntry())
				return;
			decidedMove = move;
			decidedMoveIndicatorTrs.gameObject.SetActive(true);
			decidedMoveIndicatorTrs.localPosition = (Vector2) move;
			if (autoSwitchEnabledOnDecidedMove)
				SwitchEnabledToNext ();
		}

		void HandleSwitchEnabled ()
		{
			if (Keyboard.current.tabKey.wasPressedThisFrame || (Keyboard.current.tabKey.isPressed && timesSwitchedEnabledThisTurn < 3))
				SwitchEnabledToNext ();
		}

		void SwitchEnabledToNext ()
		{
			if (!GameManager.AddStackEntry())
				return;
			bool foundThisPlayer = false;
			Player firstOtherPlayerThatDidntDecideMoveThisTurn = null;
			SetInstances ();
			for (int i = 0; i < instances.Length; i ++)
			{
				Player player = instances[i];
				if (player == this)
					foundThisPlayer = true;
				else
				{
					firstOtherPlayerThatDidntDecideMoveThisTurn = player;
					if (foundThisPlayer)
					{
						SwitchEnabled (player);
						return;
					}
				}
			}
			SwitchEnabled (firstOtherPlayerThatDidntDecideMoveThisTurn);
		}

		void SwitchEnabled (Player newActive)
		{
			if (!GameManager.AddStackEntry())
				return;
			if (newActive == null)
				return;
			if (!isDead)
			{
				enabled = false;
				trs.SetSiblingIndex(trs.parent.childCount - 1);
			}
			newActive.enabled = true;
			timesSwitchedEnabledThisTurn ++;
		}

		void OnBeforeAct ()
		{
			if (!GameManager.AddStackEntry())
				return;
			SetInstances ();
			for (int i = 0; i < instances.Length; i ++)
			{
				Player player = instances[i];
				if (!player.enabled)
					player.Register ();
			}
			List<Acter> acters = Acter.instancesDict[0];
			for (int i = 0; i < acters.Count; i ++)
			{
				Acter acter = acters[i];
				acter.cellsEnteredThisTurn.Clear();
				acter.rotationsThisTurn.Clear();
				acter.fellUnitsThisTurn = 0;
			}
		}

		bool AllPlayersDecidedMoves ()
		{
			SetInstances ();
			for (int i = 0; i < instances.Length; i ++)
			{
				Player player = instances[i];
				if (player.decidedMove == null)
					return false;
			}
			return true;
		}

		public override void Act ()
		{
			base.Act ();
			for (uint i = 1; i <= Acter.LAST_ACTER_ORDER; i ++)
			{
				List<Acter> acters = new List<Acter>();
				if (Acter.instancesDict.TryGetValue(i, out acters))
				{
					for (int i2 = 0; i2 < acters.Count; i2 ++)
					{
						Acter acter = acters[i2];
						if (!acter.actedThisTurn)
							acter.Act ();
					}
				}
			}
			GameManager.HandleMoveTiles ();
			GameManager.HandlePushers ();
			GameManager.UpdateActerRecordings ();
			for (int i = 0; i < GameManager.instance.playingActerRecordings.Count; i ++)
			{
				ActerRecording acterRecording = GameManager.instance.playingActerRecordings[i];
				acterRecording.TryToDoCurrentTurn ();
				if (acterRecording.currentTurnIndex == acterRecording.turns.Count)
					i --;
			}
			for (uint i = 0; i <= Acter.LAST_ACTER_ORDER; i ++)
			{
				List<Acter> acters = new List<Acter>();
				if (Acter.instancesDict.TryGetValue(i, out acters))
				{
					for (int i2 = 0; i2 < acters.Count; i2 ++)
					{
						Acter acter = acters[i2];
						acter.ApplyGravity ();
					}
				}
			}
			GameManager.UpdateActerRecordings ();
			for (int i = 0; i < GameManager.instance.recordingActerRecordings.Count; i ++)
			{
				ActerRecording acterRecording = GameManager.instance.recordingActerRecordings[i];
				acterRecording.turns.Add(new ActerRecording.Turn(acterRecording.acter.cellsEnteredThisTurn.ToArray(), acterRecording.acter.rotationsThisTurn.ToArray()));
			}
			BriarSource.instances = FindObjectsOfType<BriarSource>();
			for (int i = 0; i < BriarSource.instances.Length; i ++)
			{
				BriarSource briarSource = BriarSource.instances[i];
				if (briarSource != null)
					briarSource.Act ();
			}
			SetInstances ();
			for (int i = 0; i < instances.Length; i ++)
			{
				Player player = instances[i];
				player.decidedMove = null;
				player.decidedMoveIndicatorTrs.gameObject.SetActive(false);
			}
			GameManager.MakeNewGameState ();
			timesSwitchedEnabledThisTurn = 0;
			if (isDead)
				GameManager.instance.Undo ();
		}

		public override void Death ()
		{
			if (!GameManager.AddStackEntry())
				return;
			isDead = true;
			// turnIDied = GameManager.currentTurnIndex;
			if (instances.Length > 1)
			{
				DestroyImmediate(gameObject);
				SetInstances ();
				SwitchEnabledToNext ();
			}
			// else
			// {
			// 	GameManager.instance.Undo ();
			// 	timesSwitchedEnabledThisTurn = 0;
			// }
		}

		public static void SetInstances ()
		{
			instances = GameManager.CurrentGameStateGo.GetComponentsInChildren<Player>();
			for (int i = 0; i < instances.Length; i ++)
			{
				Player player = instances[i];
				if (player.isDead || player.gameObject.layer == LayerMask.NameToLayer("Recording"))
				{
					instances = instances.RemoveAt(i);
					i --;
				}
			}
		}
		
		public override void InitData ()
		{
			if (data == null)
				data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Acter.Data
		{
			public override object MakeAsset ()
			{
				Player player = Instantiate(GameManager.instance.playerPrefab, GameManager.gameStateGos[GameManager.gameStateGos.Count - 1].GetComponent<Transform>());
				Apply (player);
				return player;
			}

			public override void Apply (Asset asset)
			{
				if (asset.data == null)
					asset.data = this;
				base.Apply (asset);
			}
		}
	}
}