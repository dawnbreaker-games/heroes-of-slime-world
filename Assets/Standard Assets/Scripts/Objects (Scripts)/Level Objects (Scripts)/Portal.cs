using System;
using Extensions;
using UnityEngine;

namespace HeroesOfSlimeWorld
{
	public class Portal : Acter
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Portal teleportTo;
		public LineRenderer lineRendererToOtherPortal;
		public LineRenderer lineRendererToParent;

#if UNITY_EDITOR
		public override void OnValidate ()
		{
			base.OnValidate ();
			Init ();
		}
#endif

		public override void ApplyGravity ()
		{
		}

		public void Init ()
		{
			if (lineRendererToOtherPortal != null && trs != null && teleportTo != null && teleportTo.trs != null)
			{
				lineRendererToOtherPortal.SetPositions(new Vector3[] { trs.position, teleportTo.trs.position });
				lineRendererToOtherPortal.enabled = !teleportTo.lineRendererToOtherPortal.enabled;
			}
			else
				lineRendererToOtherPortal.enabled = false;
			if (lineRendererToParent != null)
			{
				if (trs != null && trs.parent != null && trs.parent.GetComponent<Acter>() != null)
				{
					lineRendererToParent.SetPositions(new Vector3[] { trs.position, trs.parent.position });
					lineRendererToParent.useWorldSpace = true;
					lineRendererToParent.SetUseWorldSpace (false);
					lineRendererToParent.enabled = true;
				}
				else
					lineRendererToParent.enabled = false;
			}
		}
		
		public override void InitData ()
		{
			if (data == null)
				data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetParentActerNameOfData ();
		}

		void SetParentActerNameOfData ()
		{
			Acter acter = trs.parent.GetComponent<Acter>();
			if (acter != null)
				_Data.parentActerName = acter.name;
		}

		void SetParentActerNameFromData ()
		{
			GameObject parentActerGo = GameObject.Find(_Data.parentActerName);
			if (parentActerGo != null)
				trs.SetParent(parentActerGo.GetComponent<Transform>());
		}

		[Serializable]
		public class Data : Acter.Data
		{
			public string parentActerName;

			public override object MakeAsset ()
			{
				Portal portal = Instantiate(GameManager.instance.portalPrefab, Player.instance.trs.parent);
				Apply (portal);
				return portal;
			}

			public override void Apply (Asset asset)
			{
				if (asset.data == null)
					asset.data = this;
				base.Apply (asset);
				Portal portal = (Portal) asset;
				portal.SetParentActerNameFromData ();
			}
		}
	}
}