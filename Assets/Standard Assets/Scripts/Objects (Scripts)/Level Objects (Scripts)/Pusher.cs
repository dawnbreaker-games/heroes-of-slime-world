using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace HeroesOfSlimeWorld
{
	public class Pusher : Acter
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Transform piecePrefabTrs;
		public int maxExtendAmount;
		[HideInInspector]
		public bool pushedThisTurn;
		// [HideInInspector]
		[SaveAndLoadValue]
		public int currentExtendedAmount;
		[HideInInspector]
		public int initChildCount;
#if UNITY_EDITOR
		public bool updateExtents;
		[HideInInspector]
		public LineRenderer[] lineRenderers = new LineRenderer[0];
		public FollowWaypoints.WaypointPath lineRenderersStyle;
#endif
		public static Pusher[] instances = new Pusher[0];

#if UNITY_EDITOR
		public override void OnValidate ()
		{
			base.OnValidate ();
			if (!Application.isPlaying)
				initChildCount = trs.childCount;
			if (updateExtents)
			{
				updateExtents = false;
				UpdateExtents ();
			}
		}

		void UpdateExtents ()
		{
			Vector2 _min = trs.position;
			if (trs.eulerAngles.z == 0 || trs.eulerAngles.z == 90)
				_min += (Vector2) trs.up;
			Vector2 _max = trs.position + trs.up * maxExtendAmount;
			Vector2 min = VectorExtensions.SetToMinComponents(_min, _max);
			Vector2 max = VectorExtensions.SetToMaxComponents(_min, _max);
			for (int i = 0; i < lineRenderers.Length; i ++)
			{
				LineRenderer lineRenderer = lineRenderers[i];
				if (lineRenderer != null)
					lineRenderer.RemoveLineRendererAndGameObjectIfEmpty (true);
			}
			lineRenderers = new LineRenderer[4];
			for (int i = 0; i < 4; i ++)
				lineRenderers[i] = trs.GetChild(0).gameObject.AddLineRendererOrMakeNewGameObject(lineRenderersStyle);
			Rect extentsRect = Rect.MinMaxRect(min.x, min.y, max.x, max.y);
			extentsRect = extentsRect.Expand(Vector2.one);
			extentsRect.SetLineRenderersToRectEdges (lineRenderers);
		}
#endif

		public void HandlePush ()
		{
			for (int i = 1; i < initChildCount; i ++)
			{
				Transform edgePiece = trs.GetChild(i);
				int iterationCount = maxExtendAmount;
				if (trs.up == Vector3.up)
					iterationCount ++;
				for (int i2 = 0; i2 < iterationCount; i2 ++)
				{
					if (i2 == maxExtendAmount)
					{
						Collider2D hitCollider = Physics2D.OverlapPoint(edgePiece.position + trs.up * i2, GameManager.instance.whatFalls);
						if (hitCollider != null)
						{
							Sandworm sandworm = hitCollider.GetComponent<Sandworm>();
							if (sandworm != null && Physics2D.OverlapPoint(sandworm.trs.position, sandworm.whatILandOn) != null)
								return;
							TryToPush ();
						}
					}
					else
					{
						Collider2D hitCollider = Physics2D.OverlapPoint(edgePiece.position + trs.up * i2, GameManager.instance.whatIsSolid);
						if (hitCollider != null && hitCollider.GetComponent<Transform>().parent != trs)
						{
							TryToPush ();
							return;
						}
					}
				}
			}
		}

		public bool TryToPush ()
		{
			List<Collider2D> hitColliders = new List<Collider2D>();
			for (int i = 1; i < initChildCount; i ++)
			{
				Transform edgePiece = trs.GetChild(i);
				hitColliders.AddRange(Physics2D.OverlapPointAll(edgePiece.position + trs.up * currentExtendedAmount, GameManager.instance.whatIsSolid));
			}
			for (int i = 0; i < hitColliders.Count; i ++)
			{
				Collider2D hitCollider = hitColliders[i];
				Acter acter = hitCollider.GetComponent<Acter>();
				if (acter != null)
				{
					if (!acter.TryToMoveTo((acter.trs.position + trs.up.Snap(Vector2.one)).ToVec2Int()))
						return false;
				}
				else
					return false;
			}
			for (int i = 1; i < initChildCount; i ++)
			{
				Transform edgePiece = trs.GetChild(i);
				Instantiate(piecePrefabTrs, edgePiece.position + trs.up * currentExtendedAmount, Quaternion.identity, trs);
			}
			currentExtendedAmount ++;
			pushedThisTurn = true;
			if (trs.up == Vector3.left || trs.up == Vector3.right)
			{
				hitColliders = new List<Collider2D>();
				for (int i = 1; i < currentExtendedAmount; i ++)
					hitColliders.AddRange(Physics2D.OverlapPointAll(trs.position + trs.up * i + Vector3.up, GameManager.instance.whatIsSolid));
				for (int i = 0; i < hitColliders.Count; i ++)
				{
					Collider2D hitCollider = hitColliders[i];
					Acter acter = hitCollider.GetComponent<Acter>();
					if (acter != null)
						acter.TryToMoveTo ((acter.trs.position + trs.up.Snap(Vector2.one)).ToVec2Int());
				}
			}
			return true;
		}

		public void Retract ()
		{
			currentExtendedAmount --;
			for (int i = 1; i < initChildCount; i ++)
			{
				Transform edgePiece = trs.GetChild(trs.childCount - 1);
				DestroyImmediate(edgePiece.gameObject); // TODO: The pieces that have been turned into fragile walls need to be remembered
			}
			if (trs.up == Vector3.left || trs.up == Vector3.right)
			{
				List<Collider2D> hitColliders = new List<Collider2D>();
				for (int i = 1; i < currentExtendedAmount + 1; i ++)
					hitColliders.AddRange(Physics2D.OverlapPointAll(trs.position + trs.up * i + Vector3.up, GameManager.instance.whatIsSolid));
				for (int i = 0; i < hitColliders.Count; i ++)
				{
					Collider2D hitCollider = hitColliders[i];
					Acter acter = hitCollider.GetComponent<Acter>();
					if (acter != null)
						acter.TryToMoveTo ((acter.trs.position - trs.up.Snap(Vector2.one)).ToVec2Int());
				}
			}

		}

		public override void ApplyGravity ()
		{
		}
		
		public override void InitData ()
		{
			if (data == null)
				data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetCurrentExtendedAmountOfData ();
		}

		void SetCurrentExtendedAmountOfData ()
		{
			_Data.currentExtendedAmount = (uint) currentExtendedAmount;
		}

		void SetCurrentExtendedAmountFromData ()
		{
			for (int i = 0; i < _Data.currentExtendedAmount; i ++)
				TryToPush ();
		}

		[Serializable]
		public class Data : Acter.Data
		{
			public uint currentExtendedAmount;

			public override object MakeAsset ()
			{
				Pusher pusher = Instantiate(GameManager.instance.pusherPrefab, Player.instance.trs.parent);
				Apply (pusher);
				return pusher;
			}

			public override void Apply (Asset asset)
			{
				if (asset.data == null)
					asset.data = this;
				base.Apply (asset);
				Pusher pusher = (Pusher) asset;
				pusher.SetCurrentExtendedAmountFromData ();
			}
		}
	}
}