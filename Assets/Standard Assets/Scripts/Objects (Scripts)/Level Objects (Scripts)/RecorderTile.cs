using System;
using UnityEngine;
using System.Collections.Generic;

namespace HeroesOfSlimeWorld
{
	public class RecorderTile : WallTile
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public SpriteRenderer spriteRenderer;
		public Color usedColor;
		[HideInInspector]
		[SaveAndLoadValue]
		public bool used;

		public void StartRecording (Acter acter)
		{
			ActerRecording acterRecording = new ActerRecording(acter, new List<ActerRecording.Turn>());
			GameManager.instance.recordingActerRecordings.Add(acterRecording);
			GameManager.instance.acterRecordings.Add(acterRecording);
			spriteRenderer.color = usedColor;
			used = true;
		}

		public override void InitData ()
		{
			if (data == null)
				data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetUsedOfData ();
		}

		void SetUsedOfData ()
		{
			_Data.used = used;
		}

		void SetUsedFromData ()
		{
			used = _Data.used;
			if (used)
				spriteRenderer.color = usedColor;
		}
		
		[Serializable]
		public class Data : WallTile.Data
		{
			public bool used;

			public override object MakeAsset ()
			{
				RecorderTile recorderTile = Instantiate(GameManager.instance.recorderTilePrefab, Player.instance.trs.parent);
				Apply (recorderTile);
				return recorderTile;
			}

			public override void Apply (Asset asset)
			{
				if (asset.data == null)
					asset.data = this;
				base.Apply (asset);
				RecorderTile recorderTile = (RecorderTile) asset;
				recorderTile.SetUsedFromData ();
			}
		}
	}
}