using System;
using Extensions;
using UnityEngine;

namespace HeroesOfSlimeWorld
{
	public class Sandworm : Enemy
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public LayerMask whatILandOn;
		public LayerMask whatICanEat;

		public override bool TryToMoveTo (Vector2Int position)
		{
			bool output = false;
			Collider2D hitCollider = Physics2D.OverlapPoint(position, whatICrashInto);
			if (hitCollider != null)
			{
				Enemy enemy = hitCollider.GetComponent<Enemy>();
                if (enemy != null && !enemy.actedThisTurn)
				{
					enemy.SetTargetPlayer ();
					if (Player.instances.Length == 0)
						return false;
					if (enemy.GetDesiredMove() == position - trs.position.ToVec2Int())
						enemy.Act ();
				}
			}
			hitCollider = Physics2D.OverlapPoint(position, whatICrashInto);
			if (hitCollider == null)
				output = true;
			Collider2D hitFragileWallCollider = Physics2D.OverlapPoint(position, LayerMask.GetMask("Fragile Wall"));
			if (hitFragileWallCollider != null)
				hitFragileWallCollider.GetComponent<FragileWallTile>().Death ();
			if (output)
			{
				trs.position = position.ToVec3();
                cellsEnteredThisTurn.Add(position);
                rotationsThisTurn.Add((uint) trs.eulerAngles.z);
                Physics2D.SyncTransforms();
                HandleDeath ();
				if (isDead)
					return true;
				Eat ();
				HandleTeleportAt (position);
			}
			else
				HandleRecordingAndPlaybackAt (position);
			if (!isDead)
			{
				HandleMovingGround ();
				HandleDeath ();
			}
			return output;
		}

		public override Vector2Int GetDesiredMove ()
		{
			Vector2 toPlayer = targetPlayer.trs.position - trs.position;
			Vector2Int move = toPlayer.ToVec2Int();
			if (Mathf.Abs(move.x) > Mathf.Abs(move.y))
				move.y = 0;
			else if (Mathf.Abs(move.x) < Mathf.Abs(move.y))
				move.x = 0;
			else
			{
				if (Vector2.Angle(move.SetX(0), trs.up) < Vector2.Angle(move.SetY(0), trs.up))
					move.x = 0;
				else
					move.y = 0;
			}
			if (move.x == 0)
				move.y = MathfExtensions.Sign(move.y);
			else
				move.x = MathfExtensions.Sign(move.x);
			return move;
		}

		public override void ApplyGravity ()
		{
			bool moved = true;
			for (uint i = 0; i <= GameManager.HIGHEST_FALL; i ++)
			{
				if (Physics2D.OverlapPoint(trs.position, whatILandOn.RemoveLayers("Sandworm")) != null || Physics2D.OverlapPoint(trs.position + Vector3.down, whatILandOn) != null)
					return;
				moved = TryToMoveTo((trs.position + Vector3.down).ToVec2Int());
				if (isDead)
					return;
				HandleDestroyFragileWall ();
				if (hazard != null)
					hazard.HandleKill ();
				HandleTeleportAt (trs.position.ToVec2Int());
				if (moved)
					fellUnitsThisTurn ++;
				else
					return;
			}
		}

		public void Eat ()
		{
			Collider2D hitCollider = Physics2D.OverlapPoint(trs.position, whatICanEat);
			if (hitCollider != null)
			{
				if (hitCollider.GetComponent<Transform>() == Player.instance.trs)
				{
					Player.instance.Death ();
					return;
				}
				Acter hitActer = hitCollider.GetComponent<Acter>();
				hitActer.Death ();
				if (hitActer is Bomb)
					return;
				uint nameSuffix = 0;
				while (GameObject.Find("Fragile Wall(Clone)" + nameSuffix) != null)
					nameSuffix ++;
				FragileWallTile fragileWallTile = Instantiate(GameManager.instance.fragileWallTilePrefab, trs.position, Quaternion.identity, Player.instance.trs.parent);
				fragileWallTile.name += nameSuffix;
			}
		}
		
		public override void InitData ()
		{
			if (data == null)
				data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Enemy.Data
		{
			public override object MakeAsset ()
			{
				Sandworm sandworm = Instantiate(GameManager.instance.sandwormPrefab, Player.instance.trs.parent);
				Apply (sandworm);
				return sandworm;
			}

			public override void Apply (Asset asset)
			{
				if (asset.data == null)
					asset.data = this;
				base.Apply (asset);
			}
		}
	}
}