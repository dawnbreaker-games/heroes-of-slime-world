using System;

namespace HeroesOfSlimeWorld
{
	public class Scorp : Enemy
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		
		public override void InitData ()
		{
			if (data == null)
				data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Enemy.Data
		{
			public override object MakeAsset ()
			{
				Scorp scorp = Instantiate(GameManager.instance.scorpPrefab, Player.instance.trs.parent);
				Apply (scorp);
				return scorp;
			}

			public override void Apply (Asset asset)
			{
				if (asset.data == null)
					asset.data = this;
				base.Apply (asset);
			}
		}
	}
}