using System;
using Extensions;
using UnityEngine;

namespace HeroesOfSlimeWorld
{
	public class WallTile : Acter
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public override bool TryToMoveTo (Vector2Int position)
		{
			return false;
		}

		public override void ApplyGravity ()
		{
		}

		public override void Death ()
		{
			OnBeforeDeath ();
			isDead = true;
			DestroyImmediate(gameObject);
		}

		public override void InitData ()
		{
			if (data == null)
				data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}
		
		[Serializable]
		public class Data : Acter.Data
		{
			public bool used;

			public override object MakeAsset ()
			{
				WallTile wallTile = Instantiate(GameManager.instance.wallTilePrefab, Player.instance.trs.parent);
				Apply (wallTile);
				return wallTile;
			}

			public override void Apply (Asset asset)
			{
				if (asset.data == null)
					asset.data = this;
				base.Apply (asset);
			}
		}
	}
}